using DelimitedFiles
using Plots
using LaTeXStrings
using HiRepAnalysis
gr(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=20,ms=6,framestyle=:box,marker=:auto,legend=:outerright)
pgfplotsx(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=20,ms=6,framestyle=:box,marker=:auto,legend=:outerright)
ls(s)=latexstring(s)
function paddec(x,d)
    s = string(round(x,digits=d))
    while length(last(split(s,"."))) < d
        s = s*"0"
    end
    return s
end
Δratio(x,y,Δx,Δy) = sqrt((Δx / y)^2 + (Δy*x / y^2)^2)
ratio_string(r,Δr) = L"\left(\frac{m(\rho)}{m(\pi)}\right)_{deg}\!\!\!\!= %$(errorstring(r,Δr))"
ensemble_string(β,r,Δr) = L"\beta=%$β \quad %$(ratio_string(r,Δr))"
β_string(β) = L"\beta=%$β"

function select_particle(particle,flavour,mπ,Δmπ,fπ,Δfπ,mπ0,Δmπ0,fπ0,Δfπ0,mρ,Δmρ,fρ,Δfρ,mρ0,Δmρ0,fρ0,Δfρ0)
    (particle==:pi) && (flavour==:charged) && return mπ,fπ,Δmπ,Δfπ
    (particle==:pi) && (flavour==:neutral) && return mπ0,fπ0,Δmπ0,Δfπ0
    (particle==:rho) && (flavour==:charged) && return mρ,fρ,Δmρ,Δfρ
    (particle==:rho) && (flavour==:neutral) && return mρ0,fρ0,Δmρ0,Δfρ0
end
function select_label(particle,flavour,meson1,meson2,vector1,vector2)
    (particle==:pi) && (flavour==:charged) && return meson1
    (particle==:pi) && (flavour==:neutral) && return meson2
    (particle==:rho) && (flavour==:charged) && return vector1
    (particle==:rho) && (flavour==:neutral) && return vector2
end
function select_shape(particle,flavour)
    (particle==:pi)  && (flavour==:charged) && return :rect
    (particle==:pi)  && (flavour==:neutral) && return :circle
    (particle==:rho) && (flavour==:charged) && return :utriangle
    (particle==:rho) && (flavour==:neutral) && return :diamond
end
function create_plots(name)
    plotdir = split(name,".")[1]
    data,header = readdlm(name, ',', Float64, '\n' ; header=true)
    isdir(plotdir) || mkdir(plotdir)

    # reference scale (from Bennett et.al.)
    c1 = 1:13
    c2 = 14:23
    c3 = 24:32
    c4 = 33:44
    c5 = 45:53
    c6 = 54:63
    c7 = 64:75
    c8 = 76:84
    c9 = 85:93
    cs = (c1,c2,c3,c4,c5,c6,c7,c8,c9)

    meson="pseudoscalar"
    meson_symb=L"\pi"
    vector_symb=L"\rho"
    meson1=meson_symb*L"^\pm"
    meson2=meson_symb*L"^0"
    vector1=vector_symb*L"^\pm"
    vector2=vector_symb*L"^0"

    mπ,  Δmπ  = data[:,6],  data[:,7]
    mπ0, Δmπ0 = data[:,8],  data[:,9]
    mρ,  Δmρ  = data[:,10], data[:,11]
    mρ0, Δmρ0 = data[:,12], data[:,13]
    fπ,  Δfπ  = data[:,14], data[:,15]
    fπ0, Δfπ0 = data[:,16], data[:,17]
    fρ,  Δfρ  = data[:,18], data[:,19]
    fρ0, Δfρ0 = data[:,20], data[:,21]
    mq, Δmq = data[:,22], data[:,23]

    piunits = L"[m(\pi^{deg})]"
    for i in 1:length(cs)
        c = cs[i]
        L = Int(data[last(c),1])
        T = Int(data[last(c),2])
        β = data[last(c),3]
        βlabel = replace(string(β),"."=>"")
        r  = mρ[last(c)]/mπ[last(c)]
        Δr = sqrt((Δmρ[last(c)]/mπ[last(c)])^2 + (Δmπ[last(c)]*mρ[last(c)]/mπ[last(c)]^2)^2)
        rlabel = string(round(r,digits=3))
        ensemble = ensemble_string(β,r,Δr)
        x,Δx,xlabel = 2mq[c]./mq[last(c)] .- 1, 2sqrt.((Δmq[c]./mq[last(c)]).^2 .+ (Δmq[last(c)].*mq[c]./mq[last(c)].^2).^2  ) ,ls(L"PCAC mass ratio $m_d/m_u$")
        plt4 = plot(title=ls(ensemble);ylabel=L"meson masses [$a^{-1}$]", xlabel=xlabel, size=(550,400))
        plt5 = plot(title=ls(ensemble);ylabel=L"decay constants [$a^{-1}$]", xlabel=xlabel, legend=:none, size=(550,400))
        for particle in (:pi, :rho)
            for flavour in (:neutral,:charged)
                m,f,Δm,Δf = select_particle(particle,flavour,mπ[c],Δmπ[c],fπ[c],Δfπ[c],mπ0[c],Δmπ0[c],fπ0[c],Δfπ0[c],mρ[c],Δmρ[c],fρ[c],Δfρ[c],mρ0[c],Δmρ0[c],fρ0[c],Δfρ0[c])
                label = select_label(particle,flavour,meson1,meson2,vector1,vector2)
                shape = select_shape(particle,flavour)
                scatter!(plt4,x,m,xerr=Δx,yerr=Δm,label=ls(label),markershape=shape)
                scatter!(plt5,x,f,xerr=Δx,yerr=Δf,label=ls(label),markershape=shape)
            end
        end
        savefig(plt4,joinpath(plotdir,"mass_pcac_b$(βlabel)_r$rlabel.pdf"))
        savefig(plt5,joinpath(plotdir,"decayconstant_pcac_b$(βlabel)_r$rlabel.pdf"))
    end
    for i in 1:length(cs)
        c = cs[i]
        L = Int(data[last(c),1])
        T = Int(data[last(c),2])
        β = data[last(c),3]
        βlabel = replace(string(β),"."=>"")
        r  = mρ[last(c)]/mπ[last(c)]
        Δr = sqrt((Δmρ[last(c)]/mπ[last(c)])^2 + (Δmπ[last(c)]*mρ[last(c)]/mπ[last(c)]^2)^2)
        rlabel = string(round(r,digits=3))
        ensemble = ensemble_string(β,r,Δr)
        x,Δx,xlabel = mπ[c]./mπ0[c], Δratio.(mπ[c],mπ0[c],Δmπ[c],Δmπ0[c]) ,L"m_{\pi^\pm}/m_{\pi^0}"
        plt4 = plot(title=ls(ensemble);ylabel=L"meson masses [$a^{-1}$]", xlabel=xlabel, size=(550,400))
        plt5 = plot(title=ls(ensemble);ylabel=L"decay constants [$a^{-1}$]", xlabel=xlabel, legend=:none, size=(550,400))
        for particle in (:pi, :rho)
            for flavour in (:neutral,:charged)
                m,f,Δm,Δf = select_particle(particle,flavour,mπ[c],Δmπ[c],fπ[c],Δfπ[c],mπ0[c],Δmπ0[c],fπ0[c],Δfπ0[c],mρ[c],Δmρ[c],fρ[c],Δfρ[c],mρ0[c],Δmρ0[c],fρ0[c],Δfρ0[c])
                label = select_label(particle,flavour,meson1,meson2,vector1,vector2)
                shape = select_shape(particle,flavour)
                scatter!(plt4,x,m,xerr=Δx,yerr=Δm,label=ls(label),markershape=shape)
                scatter!(plt5,x,f,xerr=Δx,yerr=Δf,label=ls(label),markershape=shape)
            end
        end
        savefig(plt4,joinpath(plotdir,"mass_piratio_b$(βlabel)_r$rlabel.pdf"))
        savefig(plt5,joinpath(plotdir,"decayconstant_piratio_b$(βlabel)_r$rlabel.pdf"))
    end


    markers  = deleteat!(Plots.supported_markers(),(1,2,4,7,9,13,16,17,18))
    color1 = (:Orange,:DarkOrange,:OrangeRed)
    color2 = (:LimeGreen,:ForestGreen,:DarkGreen)
    ensemble_sets = (1:3,4:6,7:9)
    xlabel = ls(L"flavoured vector mass $m(\rho^\pm)$"*piunits)
    for (j,ensemble_set) in enumerate(ensemble_sets)
        r_vec  = zeros(length(ensemble_set))
        Δr_vec = zeros(length(ensemble_set))
        ind = 0
        for i in ensemble_set
            c = cs[i]
            ind += 1
            r_vec[ind]  = maximum(mρ[c]./mπ[c])
            Δr_vec[ind] = sqrt((Δmρ[last(c)]/mπ[last(c)])^2 + (Δmπ[last(c)]*mρ[last(c)]/mπ[last(c)]^2)^2)
        end
        irmin = findmin(r_vec)[2]
        irmax = findmax(r_vec)[2]
        rdeg_string = L"\left(\frac{m(\rho)}{m(\pi)}\right)_{deg}\!\!\!\!="*errorstring(r_vec[irmin],Δr_vec[irmin])*" - "*errorstring(r_vec[irmax],Δr_vec[irmax])
        title = ls(L"Different $\beta=6.9,7.05,7.2$: %$rdeg_string")
        plt2 = plot(title=title,ylabel=ls(meson_symb*" masses $piunits"),xlabel=xlabel,legend=:topleft)
        plt3 = plot(title=title,ylabel=ls(vector_symb*" masses $piunits"),xlabel=xlabel,legend=:topleft)
        plt4 = plot(title=title,ylabel=ls(meson_symb*" decay constants $piunits"),xlabel=xlabel,legend=:topleft)
        plt5 = plot(title=title,ylabel=ls(vector_symb*" decay constants $piunits"),xlabel=xlabel,legend=:topleft)
        plt6 = plot(title=ls("Lines of constant physics : "*rdeg_string),ylabel=ls(L"m("*vector2*L")/m("*meson2*L")"),xlabel=ls(L"m("*meson1*L")/m("*meson2*L")"),legend=:topright)
        for flavour in (:charged,:neutral)
            for i in ensemble_set
                c = cs[i]
                s = mπ[last(c)]
                r = mρ[last(c)]/mπ[last(c)]
                Δr = sqrt((Δmρ[last(c)]/mπ[last(c)])^2 + (Δmπ[last(c)]*mρ[last(c)]/mπ[last(c)]^2)^2)
                Δs = Δmπ[last(c)]
                L = Int(data[last(c),1])
                T = Int(data[last(c),2])
                β = data[last(c),3]
                ensemble = ensemble_string(β,r,Δr)
                ratio = ratio_string(r,Δr)

                x,  Δx = mρ[c]./s, @. sqrt((Δmρ[c]/s)^2 + (Δs*mρ[c]/s^2)^2)
                x2,Δx2 = mπ0[c]./fπ0[c], sqrt.((Δmπ0[c]./fπ0[c]).^2 .+ (Δfπ0[c].*mπ0[c]./fπ0[c].^2).^2)
                
                mPS,fPS,ΔmPS,ΔfPS = select_particle(:pi,flavour,mπ[c],Δmπ[c],fπ[c],Δfπ[c],mπ0[c],Δmπ0[c],fπ0[c],Δfπ0[c],mρ[c],Δmρ[c],fρ[c],Δfρ[c],mρ0[c],Δmρ0[c],fρ0[c],Δfρ0[c])
                mV,fV,ΔmV,ΔfV = select_particle(:rho,flavour,mπ[c],Δmπ[c],fπ[c],Δfπ[c],mπ0[c],Δmπ0[c],fπ0[c],Δfπ0[c],mρ[c],Δmρ[c],fρ[c],Δfρ[c],mρ0[c],Δmρ0[c],fρ0[c],Δfρ0[c])
                labelPS = ls(select_label(:pi,flavour,meson1,meson2,vector1,vector2)*L" (%$(β_string(β)))")
                labelV = ls(select_label(:rho,flavour,meson1,meson2,vector1,vector2)*L" (%$(β_string(β)))")
                
                clr = (flavour==:neutral) ? color1[mod1(i,3)] : color2[mod1(i,3)]
                mkr = (flavour==:neutral) ? markers[i] : markers[mod1(i+div(length(markers),2),length(markers))] 
                scatter!(plt2,x,mPS./s, xerr=Δx,yerr=sqrt.((ΔmPS./s).^2 .+ (Δs.*mPS./s^2).^2) ,markercolor=clr,label=labelPS,markershape=mkr)
                scatter!(plt4,x,fPS./s, xerr=Δx,yerr=sqrt.((ΔfPS./s).^2 .+ (Δs.*fPS./s^2).^2) ,markercolor=clr,label=labelPS,markershape=mkr)
                scatter!(plt3,x,mV./s,  xerr=Δx,yerr=sqrt.((ΔmV./s).^2  .+ (Δs.*mV./s^2).^2) ,markercolor=clr,label=labelV,markershape=mkr)
                scatter!(plt5,x,fV./s,  xerr=Δx,yerr=sqrt.((ΔfV./s).^2  .+ (Δs.*fV./s^2).^2),markercolor=clr,label=labelV,markershape=mkr)
                loc = mρ0[c]./mπ0[c]
                Δloc = Δratio.(mρ0[c],mπ0[c],Δmρ0[c],Δmπ0[c]) 
                #loc = mπ[c]./mπ0[c]
                #Δloc = Δratio.(mπ[c],mπ0[c],Δmπ[c],Δmπ0[c]) 
                x3,Δx3 = mπ[c]./mπ0[c],  Δratio.(mπ[c],mπ0[c],Δmπ[c],Δmπ0[c])
                if flavour==:neutral
                    scatter!(plt6,x3,loc,xerr=Δx3,yerr=Δloc,markercolor=clr,label=L" (%$(β_string(β)))",markershape=markers[i])
                end
            end
        end
        savefig(plt2,joinpath(plotdir,"spacingMmass$j.pdf"))
        savefig(plt3,joinpath(plotdir,"spacingVectormass$j.pdf"))
        savefig(plt4,joinpath(plotdir,"spacingMdecay$j.pdf"))
        savefig(plt5,joinpath(plotdir,"spacingVectordecay$j.pdf"))
        savefig(plt6,joinpath(plotdir,"locp$j.pdf"))
    end
end
@time create_plots("2202.05191/latticeresults.csv")
