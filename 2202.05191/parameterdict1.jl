include("parameterdict.jl")
files1 = ["/Lt24Ls12beta6.9m1-0.90m2-0.$i/out/out_spectrum_mixed" for i in (50,55,60,65)]
files2 = ["/Lt24Ls14beta6.9m1-0.90m2-0.$i/out/out_spectrum_mixed" for i in (70,75,80,85,86,87,88,89)]
files3 = ["/Lt24Ls12beta7.05m1-0.835m2-0.$(i)/out/out_spectrum_mixed" for i in (50,55,60,65)]
files4 = ["/Lt24Ls14beta7.05m1-0.835m2-0.$(i)/out/out_spectrum_mixed" for i in (70,75,80,82,83)]
files5 = ["/Lt24Ls14beta7.2m1-0.78m2-0.$(i)/out/out_spectrum_mixed" for i in (50,55,60,65,70,73,75,77)]
files6 = ["/Lt24Ls12beta6.9m1-0.92m2-0.$(i)/out/out_spectrum_mixed" for i in (50,55,60,65,70,75,80)]
files7 = ["/Lt24Ls14beta6.9m1-0.92m2-0.$(i)/out/out_spectrum_mixed" for i in (85,88,90,91)]
files8 = ["/Lt24Ls14beta7.05m1-0.85m2-0.$(i)/out/out_spectrum_mixed" for i in (50,55,60,65,70,75,80,83)]
files9 = ["/Lt24Ls14beta7.2m1-0.794m2-0.$(i)/out/out_spectrum_mixed" for i in (45,50,55,60,65,70,75,77,79)]
files10 = ["/Lt24Ls12beta6.9m1-0.87m2-0.$(i)/out/out_spectrum_mixed" for i in (50,55,60,65,70,75,80,83,84,85,86)]
files11 = ["/Lt24Ls12beta7.05m1-0.80m2-0.$(i)/out/out_spectrum_mixed" for i in (45,50,55,60,65,70,75,78)]
files12 = ["/Lt24Ls12beta7.2m1-0.75m2-0.$(i)/out/out_spectrum_mixed" for i in (45,50,55,60,65,70,72,74)]

files2d = ["/Lt24Ls14beta6.9m1-0.90m2-0.$i/out/out_spectrum_mixed" for i in (90)]
files4d = ["/Lt24Ls14beta7.05m1-0.835m2-0.$(i)/out/out_spectrum_mixed" for i in (835)]
files5d = ["/Lt24Ls14beta7.2m1-0.78m2-0.$(i)/out/out_spectrum_mixed" for i in (78)]
files7d = ["/Lt24Ls14beta6.9m1-0.92m2-0.$(i)/out/out_spectrum_mixed" for i in (92)]
files8d = ["/Lt24Ls14beta7.05m1-0.85m2-0.$(i)/out/out_spectrum_mixed" for i in (85)]
files9d = ["/Lt32Ls16beta7.2m1-0.794m2-0.$(i)/out/out_spectrum_mixed" for i in (794)]
files10d = ["/Lt24Ls12beta6.9m1-0.87m2-0.$(i)/out/out_spectrum_mixed" for i in (87)]
files11d = ["/Lt24Ls12beta7.05m1-0.80m2-0.$(i)/out/out_spectrum_mixed" for i in (80)]
files12d = ["/Lt24Ls12beta7.2m1-0.75m2-0.$(i)/out/out_spectrum_mixed" for i in (75)]

th = 20
kws1 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws2 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws3 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws4 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws5 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws6 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws7 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws8 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws9 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws10 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws11 = (therm=th,step=1,autocor=true,ncut=(8,12))
kws12 = (therm=th,step=1,autocor=true,ncut=(8,12))

kws2d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws4d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws5d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws7d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws8d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws9d = (therm=100,step=1,autocor=true,ncut=(9,12))
kws10d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws11d = (therm=100,step=1,autocor=true,ncut=(8,12))
kws12d = (therm=100,step=1,autocor=true,ncut=(8,12))

filelist = (files1,files2, files2d,files3,files4,files4d,files5,files5d,files6,
            files7,files7d,files8,files8d,files9,files9d,files10,files10d,files11,
            files11d,files12,files12d)
kwslist  = (kws1,kws2,kws2d,kws3,kws4,kws4d,kws5,kws5d,kws6,kws7,kws7d,kws8,kws8d,
            kws9,kws9d,kws10,kws10d,kws11,kws11d,kws12,kws12d)

for i in eachindex(filelist)
    p = assemble_parameters(filelist[i],kwslist[i])
    prm2file(p,"parameters.csv")
end
prm = file2prm("parameters.csv";delim="\t")
