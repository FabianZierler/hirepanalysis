using HiRepAnalysis
include("parameterdict.jl")
typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4/"
datapath = "/media/storage/Fabian_Zierler/runsSp4/"

files1  = ["/Lt16Ls8beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (50,55,60,65,70,75,80,85,89)] # 86,87,88 fehlt
kws1    = (therm=100,step=1,autocor=true,ncut=(5,8))
files2  = ["/Lt20Ls10beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (70,75,80,85,89)] # 86,87,88 fehlt
kws2    = (therm=80,step=1,autocor=true,ncut=(6,10))
files3  = ["/Lt24Ls12beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (70,75,80,85,86,87,88,89)] # 86,87,88 fehlt
kws3    = (therm=80,step=1,autocor=true,ncut=(8,12))
files4  = ["/Lt16Ls8beta7.05m1-0.835m2-0.$(i)/out/out_spectrum" for i in (70,75,80,82,83)]
kws4    = (therm=100,step=1,autocor=true,ncut=(5,8))
files5  = ["/Lt20Ls10beta7.05m1-0.835m2-0.$(i)/out/out_spectrum" for i in (70,75,80,82,83)]
kws5    = (therm=100,step=1,autocor=true,ncut=(6,10))
files6  = ["/Lt24Ls12beta7.05m1-0.835m2-0.$(i)/out/out_spectrum" for i in (70,75,80,82,83)]
kws6    = (therm=100,step=1,autocor=true,ncut=(8,12))
files7  = ["/Lt20Ls10beta7.2m1-0.78m2-0.$(i)/out/out_spectrum" for i in (65,70,75,77)]
kws7    = (therm=100,step=1,autocor=true,ncut=(6,10))
files8  = ["/Lt24Ls12beta7.2m1-0.78m2-0.$(i)/out/out_spectrum" for i in (65,70,75,77)]
kws8    = (therm=70,step=1,autocor=true,ncut=(8,12))
files9  = ["/Lt20Ls10beta6.9m1-0.92m2-0.$(i)/out/out_spectrum" for i in (50,55,60,65,70,75,80,85,90,91)]
kws9    = (therm=70,step=1,autocor=true,ncut=(6,10))
files10 = ["/Lt24Ls12beta6.9m1-0.92m2-0.$(i)/out/out_spectrum" for i in (50,55,60,65,70,75)]
kws10   = (therm=70,step=1,autocor=true,ncut=(8,12))
files11 = ["/Lt20Ls10beta7.05m1-0.85m2-0.$(i)/out/out_spectrum" for i in (60,65,70,75,80,83)]
kws11   = (therm=40,step=1,autocor=true,ncut=(6,10))
files12 = ["/Lt24Ls12beta7.05m1-0.85m2-0.$(i)/out/out_spectrum" for i in (60,65,70,75,80,83)]
kws12   = (therm=40,step=1,autocor=true,ncut=(8,12))
files13 = ["/Lt20Ls10beta7.2m1-0.794m2-0.$(i)/out/out_spectrum" for i in (60,65,70,75,77,79)]
kws13   = (therm=40,step=1,autocor=true,ncut=(6,10))
files14 = ["/Lt24Ls12beta7.2m1-0.794m2-0.$(i)/out/out_spectrum" for i in (60,65,70,75,77,79)]
kws14   = (therm=40,step=1,autocor=true,ncut=(8,12))
files15 = ["/Lt24Ls14beta7.2m1-0.794m2-0.$(i)/out/out_spectrum" for i in (75,77,79)]
kws15   = (therm=40,step=1,autocor=true,ncut=(8,12))
files16 = ["/Lt16Ls8beta6.9m1-0.87m2-0.$(i)/out/out_spectrum" for i in (70,75,80,85)]
kws16   = (therm=40,step=1,autocor=true,ncut=(5,8))
files17 = ["/Lt20Ls10beta6.9m1-0.87m2-0.$(i)/out/out_spectrum" for i in (50,55,60,65,70,75,80,85)]
kws17   = (therm=40,step=1,autocor=true,ncut=(7,10))
files18 = ["/Lt20Ls10beta7.05m1-0.80m2-0.$(i)/out/out_spectrum" for i in (55,60,65,70,75,78)]
kws18   = (therm=40,step=1,autocor=true,ncut=(7,10))
files19 = ["/Lt20Ls10beta7.2m1-0.75m2-0.$(i)/out/out_spectrum" for i in (55,60,65,70,72,74)]
kws19   = (therm=40,step=1,autocor=true,ncut=(7,10))

filelist = (files1 ,files2 ,files3 ,files4 ,files5 ,files6,
            files7 ,files8 ,files9 ,files10,files11,files12,
            files13,files14,files15,files16,files17,files18,files19)
kwslist  = (kws1 ,kws2 ,kws3 ,kws4 ,kws5 ,kws6 ,kws7 ,kws8 ,kws9 ,kws10,kws11,
            kws12,kws13,kws14,kws15,kws16,kws17,kws18,kws19)

for i in eachindex(filelist)
    p = HiRepAnalysis.assemble_parameters(filelist[i],kwslist[i])
    prm2file(p,"parameters_small.csv")
end
prm = file2prm("parameters_small.csv";delim="\t")
