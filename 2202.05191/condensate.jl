using Plots, DelimitedFiles, HiRepAnalysis, LaTeXStrings, Distributions
name = "1exp_bars_jan_v20.csv"
name = "datafreeze.csv"
name = "2202.05191/latticeresults.csv"
data,header = readdlm(name, ',', Float64, '\n' ; header=true)
pgfplotsx(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=18,ms=0,framestyle=:box,marker=:none,legend=:topleft)

c,s,r,deg,beta = 14:23,0.4376,0.5517/0.4376,"1.25","7.05"
c,s,r,deg,beta = 24:32,0.3702,0.4664/0.3702,"1.25","7.2"
c,s,r,deg,beta = 45:53,0.3311,0.470/0.3311,"1.4","7.05"
c,s,r,deg,beta = 54:63,0.2874,0.4019/0.2874,"1.4","7.2"
c,s,r,deg,beta = 64:75,0.7403,0.8475/0.7403,"1.15","6.9"
c,s,r,deg,beta = 76:84,0.622,0.77/0.67,"1.15","7.05"
c,s,r,deg,beta = 85:93,0.527,0.63/0.54,"1.15","7.2"

c,s,r,deg,beta = 1:13,0.5625,0.692/0.5625,"1.25","6.9"
c,s,r,deg,beta = 33:44,0.3867,0.546/0.3867,"1.4","6.9"

mπ,  Δmπ  = data[c,6],  data[c,7]
mπ0, Δmπ0 = data[c,8],  data[c,9]
mρ,  Δmρ  = data[c,10], data[c,11]
mρ0, Δmρ0 = data[c,12], data[c,13]
fπ,  Δfπ  = data[c,14], data[c,15]
fπ0, Δfπ0 = data[c,16], data[c,17]
fρ,  Δfρ  = data[c,18], data[c,19]
fρ0, Δfρ0 = data[c,20], data[c,21]
mq, Δmq = data[c,22], data[c,23]
md, Δmd = nondeg_pcac_mass(mq,Δmq,length(c))
mu, Δmu =mq[end],Δmq[end]

GMORmπ(mπ,fπ,Σu,Σd,mu,md;Z=2)  = (mπ*fπ)^2 - 2(Σu/Z)^2*(mu*Z+md*Z)/(Σu/Z+Σd/Z)
GMORmπ0(mπ0,fπ0,Σu,Σd,mu,md;Z=2) = (mπ0*fπ0)^2 - 2(Σu/Z)^2*(mu*Σu+md*Σd)/((Σu/Z)^2+(Σd/Z)^2)

#GMORmπ(mπ,fπ,Σu,Σd,mu,md)  = (mπ*fπ)^2 - (1/2)*(mu+md)*(Σu+Σd)
#GMORmπ0(mπ0,fπ0,Σu,Σd,mu,md) = (mπ0*fπ0)^2 - (1/2)*(mu*Σu+md*Σd)*(Σu+Σd)^2/(Σu^2+Σd^2)

function f1(mπ,fπ,mavg,mπ_deg,fπ_deg,mavg_deg,Σd)
    mu = mavg_deg
    md = 2mavg - mu
    Σu = (mπ_deg*fπ_deg)^2/(2mu)
    GMORmπ(mπ,fπ,Σu,Σd,mu,md)
end
function f2(mπ0,fπ0,mavg,mπ_deg,fπ_deg,mavg_deg,Σd)
    mu = mavg_deg
    md = 2mavg - mu
    Σu = (mπ_deg*fπ_deg)^2/(2mu)
    GMORmπ0(mπ0,fπ0,Σu,Σd,mu,md)
end
function GMOR_estimate(mπ,fπ,mπ0,fπ0,mavg,mπ_deg,fπ_deg,mavg_deg,Δmπ,Δfπ,Δmπ0,Δfπ0,Δmavg,Δmπ_deg,Δfπ_deg,Δmavg_deg,Σd)
    # add decay constant error of 10% that should account for 10%
    Δfπ *= 1.1
    Δfπ_deg *= 1.1
    # initialize normal distributions of quantities
    mπ_dist = Normal(mπ,Δmπ)
    mπ0_dist = Normal(mπ0,Δmπ0)
    fπ_dist = Normal(fπ,Δfπ)
    fπ0_dist = Normal(fπ0,Δfπ0)
    mavg_dist = Normal(mavg,Δmavg)
    mπ_deg_dist = Normal(mπ_deg,Δmπ_deg)
    fπ_deg_dist = Normal(fπ_deg,Δfπ_deg)
    mavg_deg_dist = Normal(mavg_deg,Δmavg_deg)
    # draw n times from each distribution
    n = 10_000
    f1s = zeros(n)
    f2s = zeros(n)
    for i in 1:n
        f1s[i] = f1(rand(mπ_dist),rand(fπ_dist),rand(mavg_dist),rand(mπ_deg_dist),rand(fπ_deg_dist),rand(mavg_deg_dist),Σd)
        f2s[i] = f2(rand(mπ0_dist),rand(fπ0_dist),rand(mavg_dist),rand(mπ_deg_dist),rand(fπ_deg_dist),rand(mavg_deg_dist),Σd)
    end
    fit1 = fit(Normal,f1s)
    fit2 = fit(Normal,f2s)
    GMOR1, ΔGMOR1 = fit1.μ, fit1.σ
    GMOR2, ΔGMOR2 = fit2.μ, fit2.σ
    return GMOR1, GMOR2, ΔGMOR1, ΔGMOR2
end

for j in 0:length(mπ)-1
    i = length(mπ) - j
    
    x = (0.001:0.0002:0.4).^3
    r1 = GMOR_estimate.(mπ[i],fπ[i],mπ0[i],fπ0[i],mq[i],mπ[end],fπ[end],mq[end],Δmπ[i],Δfπ[i],Δmπ0[i],Δfπ0[i],Δmq[i],Δmπ[end],Δfπ[end],Δmq[end],x)
    # smoothen errors
    GMOR1 = getindex.(r1,1)
    GMOR2 = getindex.(r1,2)
    ΔGMOR1 = getindex.(r1,3)
    ΔGMOR2 = getindex.(r1,4)
    plot(cbrt.(x),GMOR1,ribbon=ΔGMOR1,label=L"\pi^\pm",lw=3)
    plot!(cbrt.(x),GMOR2,ribbon=ΔGMOR2,label=L"\pi^0",lw=3)
    plot!(cbrt.(x),zero(x),label="",lw=3,ls=:dot,color=:black)
    plot!(ylabel = L"GMOR$(m_u,m_d,\mu_u,x)$",framestyle=:box)
    ratio  = md[i]/mu
    Δratio = sqrt( (Δmd[i]/mu)^2 + (Δmu*md[i]/mu^2)^2 )
    @show ratio,Δratio
    if ratio == 1.0
        test = "1"
    else
        test = errorstring(ratio,Δratio)
    end
    sqrt( (Δmd[i]/mu)^2 + (Δmu*md[i]/mu^2)^2)
    plot!(title=L"$\!\!\!\!\!\!\!\! \beta = %$(beta) \quad (m_\rho/m_\pi)_{\rm deg}  \approx \! %$(deg) \quad \left(m_d / m_u\right)_{\rm PCAC}  \! = \! %$(test)$")
    plot!(xlabel=L"x [a^{-1}]",size=(550,400))
    savefig("condensates/GMOR_$(beta)_$(deg)_$j.pdf")
end
