function assemble_parameters(files,kws)
    prm = Dict{String,Any}[]
    for file in files
        for Γ in ("g0g5","g1")
            d = Dict("file"=>file, "Γ"=>Γ, "therm"=>kws.therm,"step"=>kws.step,
                "cut1"=>kws.ncut[1],"cut2"=>kws.ncut[2],"autocor"=>kws.autocor)
            push!(prm,d)
        end
    end
    return prm
end
function prm2file(prms,file)
    open(file, "a") do io
        #seekstart(io)
        for prm in prms
            write(io,"$(prm["file"])\t")
            write(io,"$(prm["Γ"])\t")
            write(io,"$(prm["therm"])\t")
            write(io,"$(prm["step"])\t")
            write(io,"$(prm["autocor"])\t")
            write(io,"$(prm["cut1"])\t")
            write(io,"$(prm["cut2"])\n")
        end
    end
end
function file2prm(file;delim="\t")
    prm = Dict{String,Any}[]
    io = open(file,"r")
    for line in readlines(io)
        l = replace(line," "=>"")
        v = split(l,delim,keepempty=false)
        file = string(v[1])
        Γ = string(v[2])
        therm = parse(Int,v[3])
        step = parse(Int,v[4])
        autocor = parse(Bool,v[5])
        ncut1 = parse(Int,v[6])
        ncut2 = parse(Int,v[7])
        d = Dict("file"=>file, "Γ"=>Γ, "therm"=>therm,"autocor"=>autocor,"step"=>step,"cut1"=>ncut1,"cut2"=>ncut2)
        push!(prm,d)
    end
    return prm
end
