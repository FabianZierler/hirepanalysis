using DelimitedFiles, Plots, LaTeXStrings, HiRepAnalysis
name = "test_analysis"
data,header = readdlm(name, ';', Float64, '\n' ; header=true)

L, T, β  = Int.(data[:,1]),  Int.(data[:,2]), data[:,3]
mq1, mq2 = data[:,4],  data[:,5]
mπ  = errorstring.(data[:,6],data[:,7])
mπ0 = errorstring.(data[:,8],data[:,9])
mρ  = errorstring.(data[:,10], data[:,11])
mρ0 = errorstring.(data[:,12], data[:,13])
fπ  = errorstring.(data[:,14], data[:,15])
fπ0 = errorstring.(data[:,16], data[:,17])
fρ  = errorstring.(data[:,18], data[:,19])
fρ0 = errorstring.(data[:,20], data[:,21])
mAWI = errorstring.(data[:,22], data[:,23])
P = errorstring.(data[:,24], data[:,25])

function start_table(file)
    open(file,"a") do io
        write(io,"\\begin{table} \n")
        write(io,"\t \\centering \n")
        write(io,"\t \\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|} \n")
        write(io,"\t\t \\hline \\hline \n")
        write(io,"\t\t \$\\beta\$ & \$T\$ & \$L\$ & \$am_u^0\$ & \$am_d^0\$ & ")
        write(io,"\$ <P> \$ & \$am(\\pi^A)\$ & \$am(\\pi^C)\$ & \$af(\\pi^A)\$ & \$af(\\pi^C)\$ & ")
        write(io,"\$am(\\rho^M)\$ & \$am(\\rho^N)\$ & \$af(\\rho^M)\$ & \$af(\\rho^N)\$ &")
        write(io,"\$a \\bar m_\\text{PCAC}\$ \\\\ \n")
        write(io,"\t\t \\hline \n")
    end
end
function end_table(file)
    open(file,"a") do io
        write(io,"\t\t \\hline \\hline \n")
        write(io,"\t \\end{tabular} \n")
        write(io,"\\end{table} \n")
    end
end
function write_data(file,β,T,L,mq1,mq2,mπ,mπ0,fπ,fπ0;start=1,stop=typemax(Int))
    β0 = first(β)
    open(file,"a") do io
        for i in start:min(length(β),stop)
            if β[i] != β0
                write(io,"\t\t \\hline \n")
                β0 = β[i]
            end
            write(io,"\t\t $(β[i]) & $(T[i]) & $(L[i]) & $(mq1[i]) & $(mq2[i]) &")
            write(io," $(P[i]) & $(mπ[i]) & $(mπ0[i]) & $(fπ[i]) & $(fπ0[i]) & ")
            write(io," $(mρ[i]) & $(mρ0[i]) & $(fρ[i]) & $(fρ0[i]) & $(mAWI[i]) \\\\ \n ")
        end
    end
end

file = "table"
start_table(file)
write_data(file,β,T,L,mq1,mq2,mπ,mπ0,fπ,fπ0,start=1,stop=32)
end_table(file)
start_table(file)
write_data(file,β,T,L,mq1,mq2,mπ,mπ0,fπ,fπ0,start=33,stop=63)
end_table(file)
start_table(file)
write_data(file,β,T,L,mq1,mq2,mπ,mπ0,fπ,fπ0,start=64,stop=93)
end_table(file)
