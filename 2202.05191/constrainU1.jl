using Plots
using LaTeXStrings
latticestring(Λ) = "$(Λ[1])×$(Λ[2])³"
decimals(s) = length(last(split(s,".")))
pgfplotsx(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=17,framestyle=:box,legend=:bottomleft)

x = 10 .^ (-2.005:0.01:4.0)
xticks_x  = 10 .^ (-4:1:4.0)
yticks_y  = 10 .^ (-30:1:6.0)

c = 3/(2π^2)
y(x,r) = c*log(x)/(x^2-1)/r
Δy(x,r,Δr) = Δr*c*log(x)/(x^2-1)/r^2
ylab = L"\kappa / m_\rho^4"
xlab = L"m_V / m_\rho "

plt = plot(yticks=yticks_y,xticks=xticks_x, xlabel=xlab ,ylabel=ylab, yscale=:log10, xscale=:log10)
plot!(plt,x,y.(x,7.37),label=L"$Sp(4)_c :$ $m_\rho / f_\pi = $ 7.37(15) ",ribbon=Δy.(x,7.37,0.15),ms=0)
plot!(plt,x,y.(x,8.08),label=L"$Sp(4)_c :$ $m_\rho / f_\pi = $ 8.08(32) ",ribbon=Δy.(x,8.08,0.32),ms=0)
plot!(plt,x,y.(x,8.34),label=L"QCD : $m_\rho / f_\pi = $ 8.34(2)",ribbon=Δy.(x,8.34,0.02),ms=0)
plot!(plt,title=L"$U(1)'$ breaking parameter $\kappa$ against dark photon mass $m_V$")
#savefig("kappa_lattice.pdf")
