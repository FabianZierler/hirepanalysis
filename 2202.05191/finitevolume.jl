using HiRepAnalysis
using LsqFit
using Plots
using LaTeXStrings
datapath = "/media/storage/Fabian_Zierler/runsSp4"
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
datapath = "/run/user/1002/gvfs/smb-share:server=143.50.77.103,share=home/runsSp4/"

typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"

function finitevolume(L,m,Δm)
    @. model(L,p)  = p[1]*(1+abs(p[2])*exp(-L*p[1])/abs(L*p[1])^(3/2))
    init = [1,0.1]
    #@. model(L,p) = p[1] + p[2]*exp(-p[3]*L)/L
    #init = [1.0,0.5,1.0]
    fit  = curve_fit(model,L,m,init)
    fitp = curve_fit(model,L,m+Δm,init)
    fitm = curve_fit(model,L,m-Δm,init)
    @assert fit.converged*fitp.converged*fitm.converged
    return fit,fitp,fitm, model
end
function finitevolume(L,m,Δm,mGS_inf)
    @. model(L,p)  = p[1]*(1+abs(p[2])*exp(-L*mGS_inf)/abs(L*mGS_inf)^(3/2))
    init = [m[end],0.1]
    fit  = curve_fit(model,L,m,Δm.^(-2),init)
    fitp = curve_fit(model,L,m+Δm,init)
    fitm = curve_fit(model,L,m-Δm,init)
    @assert fit.converged*fitp.converged*fitm.converged
    return fit,fitp,fitm, model
end
function finitevolume(L,m,Δm,mGS,ΔmGS)
    fit0 = finitevolume(L,mGS,ΔmGS)[1]
    mGS_inf = fit0.param[1]
    return finitevolume(L,m,Δm,mGS_inf)
end
function finitevolumeplot(L,m,Δm;kws...)
    fit, fitp, fitm, model = finitevolume(L,m,Δm)
    return finitevolumeplot(L,m,Δm,fit, fitp, fitm, model;kws...)
end
function finitevolumeplot(L,m,Δm,mGS,ΔmGS;kws...)
    fit, fitp, fitm, model = finitevolume(L,m,Δm,mGS,ΔmGS)
    return finitevolumeplot(L,m,Δm,fit, fitp, fitm, model;kws...)
end
function finitevolumeplot(L,m,Δm,fit, fitp, fitm, model;title="")
    Linv = inv.(L)
    Linv_max = 0.05
    xinv = Linv_max:0.001:1.05/minimum(L)
    # evaluarte model functions for fittet coefs
    x = inv.(xinv)
    y = model(x,coef(fit))
    yp = model(x,coef(fitp))
    ym = model(x,coef(fitm))
    # infinite volume mass
    m∞  = fitmass(fit)
    #Δm∞ = abs(fitmass(fitp) - fitmass(fitm))/2 # first(stderror(fit))
    Δm∞ = max(abs(fitmass(fitp)-fitmass(fit)),abs(fitmass(fitm)-fitmass(fit)))
    # plotting
    tickstrings =  [ L"\frac{1}{%$(L[i])}" for i in eachindex(L)]
    ticks = (Linv, tickstrings )
    plt = plot(xinv,y,label="fit ",ribbon=(abs.(yp-y),abs.(ym-y)),size=(400,435),marker=:none)
    scatter!(plt,Linv,yerr=Δm,m,label="fitted data",marker=:circle)
    scatter!(plt,(0.05,m∞),yerr=Δm∞,label=L"L \rightarrow \infty :  m=%$(errorstring(m∞,Δm∞))",marker=:square)
    plot!(plt,xlabel="inverse spatial extent 1/L",ylabel="",title=title)
    plot!(plt,xticks=ticks,legend=:topleft)
    return plt
end

pgfplotsx(legendfontsize=18,labelfontsize=22,tickfontsize=16,titlefontsize=16,ms=8,framestyle=:box)

sup= L"^0"
meson=L"\pi"*sup
vector=L"\rho"*sup



βs, m1, m2 = "7.2", "-0.794", "-0.79"
Ls = [10,12,14,16]
Ts = [20,24,24,32]
ncuts = [(6,10),(8,12),(9,12),(11,16)]
therms = (100,100,50,50)
steps = (1,1,1,1)

βs, m1, m2 = "6.9", "-0.92", "-0.90"
βs, m1, m2 = "7.2", "-0.78", "-0.77"
Ls = [8,10,12,14,16]
Ts = [16,20,24,24,32]
ncuts = [(4,8),(6,10),(8,12),(9,12),(11,16)]
therms = (100,100,100,50,50)
steps = (1,1,1,1,1)

βs, m1, m2 = "6.9", "-0.90", "-0.89"
Ls = [8,10,12,14]
Ts = [16,20,24,24]
ncuts = [(4,8),(6,10),(8,12),(9,12)]
therms = (100,100,100,50)
steps = (1,1,1,1)

keyPS, keyV = "g5", "g1"
mπ, mπ0, Δmπ, Δmπ0 = zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls))
mM, mM0, ΔmM, ΔmM0 = zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls))
fπ, fπ0, Δfπ, Δfπ0 = zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls))
fM, fM0, ΔfM, ΔfM0 = zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls)), zeros(length(Ls))

for j in eachindex(Ls)
    file = datapath*"Lt$(Ts[j])Ls$(Ls[j])beta$(βs)m1$(m1)m2$(m2)/out/out_spectrum"
    if !isfile(file)
        file = datapath*"Lt$(Ts[j])Ls$(Ls[j])beta$(βs)m1$(m1)m2$(m2)/out/out_spectrum_mixed"
    end
    kws = (therm=therms[j],step=steps[j],autocor=true,ncut=ncuts[j],error=:hist,nexp2=false)
    mπ[j], Δmπ[j], fπ[j], Δfπ[j] = meson_mass_decay(file,keyPS,typeU,typeD;kws...)
    mM[j], ΔmM[j], fM[j], ΔfM[j] = meson_mass_decay(file,keyV,typeU,typeD;kws...)
end
c = 1:length(Ls)
cut(L,c) = getindex.(Ref(L),c)
plt1 = finitevolumeplot(cut(Ls,c),cut(mπ,c),cut(Δmπ,c),title=L"am(%$meson): \beta=%$(βs) $(am_u^0, am_d^0)$ = (%$m1,%$m2)" )
plt2 = finitevolumeplot(cut(Ls,c),cut(mM,c),cut(ΔmM,c),cut(mπ,c),cut(Δmπ,c),title=L"am(%$vector): \beta=%$(βs) $(am_u^0, am_d^0)$ = (%$m1,%$m2)")
dir = "finite_volume"
isdir(dir) || mkdir(dir)
savefig(plt1,dir*"/pion_b$(βs)_m1$(m1)m2$(m2).pdf")
savefig(plt2,dir*"/vector_b$(βs)_m1$(m1)m2$(m2).pdf")

