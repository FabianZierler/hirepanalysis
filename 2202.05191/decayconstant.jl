using HiRepAnalysis
include("parameterdict.jl")
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
datapath = "/media/storage/Fabian_Zierler/runsSp4"
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
datapath = "/home/zierler_fabian/Documents/rsync/rsyncVSC/runsSp4/"
datapath = "/run/user/1002/gvfs/smb-share:server=143.50.77.103,share=home/runsSp4/"
# names for parsing
typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"

ZA(plaq,β) = 1 + (5/4)*(-12.82-3)*8/(16*pi^2)/(β*plaq)
ZV(plaq,β) = 1 + (5/4)*(-12.82-7.75)*8/(16*pi^2)/(β*plaq)

function write_csv(prmfile,name,datapath)
    prms = file2prm(prmfile)
    for i in 145:2:length(prms)
        # input paramters
        @show file = datapath*string(prms[i]["file"])
        Λ,q,β = fileinfo(file)
        mq1, mq2 = try 
            quarkmasses(file)
        catch
            quarkmasses(file)[1],quarkmasses(file)[1]
        end
        T, L = Λ[1], Λ[2]
        t0 = (T==32) ? 0 : 50
        f0 = (T==32) ? 1 : 3
        # shared kws
        kws = (therm=prms[i]["therm"]+t0, step=f0*prms[i]["step"], autocor=prms[i]["autocor"], error=:hist, nexp2=false )
        # construct fitting ranges for pions and vectors
        cut_g5 = ( prms[i]["cut1"]  , prms[i]["cut2"])
        cut_g1 = ( prms[i+1]["cut1"], prms[i+1]["cut2"])
        key1 = "g0g5"
        key2 = "g1"
        @assert prms[i]["Γ"] == key1
        @assert prms[i+1]["Γ"] == key2
        mπ0,Δmπ0,fπ0,Δfπ0 = HiRepAnalysis.meson_mass_decay_select(file,key1,typeD,typeU;kws...,ncut=cut_g5)[1:4]
        mπ ,Δmπ ,fπ ,Δfπ  = HiRepAnalysis.meson_mass_decay_select(file,key1,typeUD     ;kws...,ncut=cut_g5)[1:4]
        mρ0,Δmρ0,fρ0,Δfρ0 = HiRepAnalysis.meson_mass_decay_select(file,key2,typeD,typeU;kws...,ncut=cut_g1)[1:4]
        mρ ,Δmρ ,fρ ,Δfρ  = HiRepAnalysis.meson_mass_decay_select(file,key2,typeUD     ;kws...,ncut=cut_g1)[1:4]
        # calculate AWI mass
        kwsAWI = (therm=kws.therm,step=kws.step,autocor=kws.autocor,ncut=cut_g5[1])
        mAWI, ΔmAWI = awi_mass(file,typeUD;kwsAWI...)
        # TODO: calculate pion mass and decay constant from mixed correlator
        # renormalize decay constants
        P,ΔP = average_plaquette(file;therm=kws.therm,step=kws.step,autocor=kws.autocor)
        fπ  *= ZA(P,β);  fπ0 *= ZA(P,β);  Δfπ *= ZA(P,β);  Δfπ0*= ZA(P,β)
        fρ  *= ZV(P,β);  fρ0 *= ZV(P,β);  Δfρ *= ZV(P,β);  Δfρ0*= ZV(P,β)
        # write to file
        open(name, "a") do io
            if iszero(position(io))
                write(io, "L$(repeat(' ',3))T$(repeat(' ',3))β$(repeat(' ',5))mq1$(repeat(' ',5))mq2$(repeat(' ',5))mπ$(repeat(' ',20))Δmπ$(repeat(' ',19))mπ0$(repeat(' ',19))Δmπ0$(repeat(' ',18))mρ$(repeat(' ',20))Δmρ$(repeat(' ',19))mρ0$(repeat(' ',19))Δmρ0$(repeat(' ',18))fπ$(repeat(' ',20))Δfπ$(repeat(' ',19))fπ0$(repeat(' ',19))Δfπ0$(repeat(' ',18))fρ$(repeat(' ',20))Δfρ$(repeat(' ',19))fρ0$(repeat(' ',19))Δfρ0$(repeat(' ',18))mAWI$(repeat(' ',18))ΔmAWI$(repeat(' ',18))P$(repeat(' ',18))ΔP\n")
            end
            write(io,rpad(L,2)*"; "*rpad(T,2)*"; "*rpad(β,4)*"; "*rpad(mq1,6)*"; "*rpad(mq2,6)*"; "*rpad(mπ,20)*"; "*rpad(Δmπ,20)*"; "*rpad(mπ0,20)*"; "*rpad(Δmπ0,20)*"; "*rpad(mρ,20)*"; "*rpad(Δmρ,20)*"; "*rpad(mρ0,20)*"; "*rpad(Δmρ0,20)*"; "*rpad(fπ,20)*"; "*rpad(Δfπ,20)*"; "*rpad(fπ0,20)*"; "*rpad(Δfπ0,20)*"; "*rpad(fρ,20)*"; "*rpad(Δfρ,20)*"; "*rpad(fρ0,20)*"; "*rpad(Δfρ0,20)*"; "*rpad(mAWI,20)*"; "*rpad(ΔmAWI,20)*"; "*rpad(P,20)*"; "*rpad(ΔP,20)*"\n")
        end
    end
end
function plot_fits(prmfile,datapath;start=0)
    prms = file2prm(prmfile)
    for i in 2*start+1:2:length(prms)
        @show file = datapath*string(prms[i]["file"])
        Λ,q,β = fileinfo(file)
        mq1, mq2 = quarkmasses(file)
        T, L = Λ[1], Λ[2]
        kws = (therm=prms[i]["therm"], step=prms[i]["step"], autocor=prms[i]["autocor"], error=:hist, nexp2=false )
        cut_g5 = ( prms[i]["cut1"]  , prms[i]["cut2"])
        cut_g1 = ( prms[i+1]["cut1"], prms[i+1]["cut2"])
        plt1 = HiRepAnalysis.plot_decay_fit(file,typeUD;key="g0g5",ncut=cut_g5,kws...)
        plt2 = HiRepAnalysis.plot_decay_fit(file,typeUD;key="g1"  ,ncut=cut_g1,kws...)
        plot!(plt1,title="β=$β T=$T L=$L mqs = $mq1, $mq2")
        plot!(plt2,title="β=$β T=$T L=$L mqs = $mq1, $mq2")
        return plt1, plt2
    end
end
write_csv("parameters.csv","nonsinglets",datapath)