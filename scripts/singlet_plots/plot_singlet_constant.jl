using Plots
using HiRepAnalysis
using LinearAlgebra
using LaTeXStrings
using DelimitedFiles
pgfplotsx(legendfontsize=12,labelfontsize=20,tickfontsize=14,titlefontsize=16,ms=4,framestyle=:box,legend=:outerright)
gr(legendfontsize=12,labelfontsize=20,tickfontsize=20,titlefontsize=18,ms=4,framestyle=:box,legend=:topright,size=(800,500))
plotly(legendfontsize=24,labelfontsize=24,tickfontsize=24,titlefontsize=22,ms=4,framestyle=:box,legend=:outerright,size=(800,500))
pgfplotsx(legendfontsize=18,labelfontsize=24,tickfontsize=24,titlefontsize=22,ms=4,framestyle=:box,legend=:outerright,size=(800,500))
pgfplotsx(legendfontsize=20,labelfontsize=24,tickfontsize=18,titlefontsize=22,ms=4,framestyle=:box,legend=:outerright,size=(800,500))

function errorbars_semilog(x,Δx)
    lower = similar(Δx)
    for i in eachindex(x)
        lower[i] = ifelse(x[i] > Δx[i],Δx[i],x[i]-1E-15)
    end
    bars = (lower,Δx)
    return bars
end
function fixed_Q_path(fullpath,Q;absQ=false)
    dir, file = splitdir(fullpath)
    if absQ
        file_fixed_Q = splitext(file)[1]*"_absQ$(abs(Q)).hdf"
    else
        file_fixed_Q = splitext(file)[1]*"_Q$Q.hdf"
    end
    path_fixed_Q = joinpath(dir,"fixedQ",file_fixed_Q)
    return path_fixed_Q
end
parse_fit_intervall(s) = isa(s,Integer) ? s : Tuple(parse.(Int,split(s[2:end-1],',')))
function read_prm(file,path,i)
    prm = readdlm(file,';')
    odir = joinpath(path,prm[i,1],"out")
    hits = prm[i,2]
    fitη,fitπ,fitσ,fita0,fitρ = parse_fit_intervall.(prm[i,3:7])
    fileM, fileS = joinpath(odir,"out_spectrum.hdf5"), joinpath(odir,prm[i,9])
    group,name,vsub,deriv,gs_sub,constant = prm[i,10:15]
    return odir, hits, fitη, fitπ, fitσ, fita0, fitρ ,fileM, fileS, group, name, vsub, deriv, gs_sub, constant
end
function singlet_analysis(fileM,fileS,hits,vsub,key,cut,gs_sub,sigma)
    type  = "DISCON_SEMWALL SINGLET" 
    typeM = "DEFAULT_SEMWALL TRIPLET"
    T, L = latticesize(fileM)[1:2]
    rescale = (L^3)^2 /L^3
    # disconnected contributions from code in /Spectrum/
    C_dis_MC = disconnected_eta_MC(fileS,type,hits;maxhits=hits,rescale,vsub,key="$(key)_disc_re")
    C_con_MC = correlators(fileM,typeM,key)
    HiRepAnalysis._rescale_corrs!(C_con_MC, L)
    return HiRepAnalysis.singlet_jackknife_corr(C_con_MC,C_dis_MC,cut;gs_sub,sigma)
end

prmfile = "DatafreezeSinglets/parameters/param_deriv.csv"

# Compare eta vs pion correlator in presence of a constant
plt_corr_pi_eta = plot()
odir,hits,fitη,fitπ,fitσ,fita0,fitρ,fileM,fileS,group,name,vsub,deriv,gs_sub,constant = read_prm(prmfile,path,11)
fileid = string(joinpath(splitpath(odir)[end-2:end-1]))

@show fileM
Nconf, m, G, β = nconfigs(fileM),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)

T, L = latticesize(fileM)[1:2]
typeM, type = "DEFAULT_SEMWALL TRIPLET", "DISCON_SEMWALL SINGLET" 
rescale = (L^3)^2 /L^3

# parameter of configurations
Nconf, m, G, β = nconfigs(fileM),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
# connected pion correlator
Cπ, ΔCπ2 = average_correlator(fileM,"g5",typeM)[1:2]
HiRepAnalysis._rescale_corrs!(Cπ, ΔCπ2, L)
ΔCπ = sqrt.(ΔCπ2)
# connected eta' correlator
gs_sub = false # do not perform ground state subtraction 
C, ΔC, Cd, ΔCd = singlet_analysis(fileM,fileS,hits,vsub,"g5",fitπ,gs_sub,false)
#title for plotting
title = L"$ %$(T)\times %$(L)^3, \beta=%$β, %$G, m_q=%$(first(m))$, $n_{\rm src} = %$hits$"      
label = L"C_{\eta'}(t)"
plot!(plt_corr_pi_eta,title=title,legend=:top,yaxis=:log10)
scatter!(plt_corr_pi_eta,2Cπ,yerr=errorbars_semilog(2Cπ,2ΔCπ),label=L"2C_{\pi}(t)")
scatter!(plt_corr_pi_eta,C,yerr=errorbars_semilog(C,ΔC),label=label,markershape=:diamond,ms=5)
plot!(plt_corr_pi_eta,yticks=10.0.^(-4:0),xticks=2:2:T,xlabel=L"t")

# Compare eta' correlator for different fixed Q
plt_corr_eta_fQ = plot()
odir,hits,fitη,fitπ,fitσ,fita0,fitρ,fileM,fileS,group,name,vsub,deriv,gs_sub,constant = read_prm(prmfile,11)
odir,hits,fitη,fitπ,fitσ,fita0,fitρ,fileM,fileS,group,name,vsub,deriv,gs_sub,constant = read_prm(prmfile,23)
fileid = string(joinpath(splitpath(odir)[end-2:end-1]))

absQ = true  
markers  = deleteat!(Plots.supported_markers(),(1,2,4,7,9,13,14,16,17,18))
for Q in [1,2,3,4]
    @show Q
    fileS_Q = fixed_Q_path(fileS,Q;absQ)
    fileM_Q = fixed_Q_path(fileM,Q;absQ)

    T, L = latticesize(fileM)[1:2]
    typeM, type = "DEFAULT_SEMWALL TRIPLET", "DISCON_SEMWALL SINGLET" 
    rescale = (L^3)^2 /L^3

    # parameter of configurations
    @show fileM_Q
    Nconf, m, G, β = nconfigs(fileM_Q),quarkmasses(fileM_Q),gaugegroup(fileM_Q),couplingβ(fileM_Q)

    # connected eta' correlator
    gs_sub = false # do not perform ground state subtraction 
    C, ΔC, Cd, ΔCd = singlet_analysis(fileM_Q,fileS_Q,hits,vsub,"g5",fitπ,gs_sub,false)
    #title for plotting
    title = L"$ %$(T)\times %$(L)^3, \beta=%$β, %$G, m_q=%$(first(m))$, $n_{\rm src} = %$hits$"
    if absQ          
        label = L"C_{\eta'}(t), |Q|=%$(Q), N_{\rm cfg} = %$Nconf" 
    else
        label = L"C_{\eta'}(t), Q=%$(Q), N_{\rm cfg} = %$Nconf" 
    end
    plot!(plt_corr_eta_fQ,title=title,legend=:top,yaxis=:log10)
    scatter!(plt_corr_eta_fQ,C,yerr=errorbars_semilog(C,ΔC),label=label,markershape=markers[Q],ms=5)
    plot!(ylims=(max(1E-5,minimum(C)/10),ylims(plt_corr_eta_fQ)[2]))
    plot!(plt_corr_eta_fQ,yticks=10.0.^(-4:0),xticks=2:2:T,xlabel=L"t")
end
savefig(plt_corr_eta_fQ,"figuresSinglets/fixedQ_correlator.pdf")
savefig(plt_corr_pi_eta,"figuresSinglets/Constant_in_eta_shifted.pdf")