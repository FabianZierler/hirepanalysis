using HiRepAnalysis
using HDF5
function writehdf5_spectrum_disconnected(file,type,nhits;filterkey=false,key_pattern="",fnid="",only_channels=nothing,corronly=false,abspath="")
    @show file
    # filename
    filename = joinpath(abspath,joinpath(splitpath(file)[end-3:end]))
    path = joinpath(abspath,joinpath(splitpath(file)[end-3:end-1]))
    ispath(path) || mkpath(path)
    hdf5fn = filename*fnid*".hdf5"
    isfile(hdf5fn) && rm(hdf5fn)
    # save other relevant quantities
    if !corronly
        h5write(hdf5fn,"plaquette",plaquettes(file))
        h5write(hdf5fn,"configurations",confignames(file))
        h5write(hdf5fn,"gauge group",gaugegroup(file))
        h5write(hdf5fn,"beta",couplingβ(file))
        h5write(hdf5fn,"quarkmasses",quarkmasses(file))
        h5write(hdf5fn,"lattice",latticesize(file))
        h5write(hdf5fn,"lofgile",filename)
        h5write(hdf5fn,"sources",nhits)
    end
    # read correlator data
    c = correlators(file,type;withsource=true,average=false,filterkey,key_pattern)
    if isnothing(only_channels)
        channels = unique(collect(keys(c[1])))
    else
        channels = intersect(unique(collect(keys(c[1]))),only_channels)
    end
    for Γ in channels
        d = getindex.(c,Γ)
        dat = flatten_disc(d,nhits;rescale=1)
        h5write(hdf5fn,type*"_"*Γ,dat)
    end
end
function writehdf5_spectrum_connected(file,type;abspath="")
    fnid = ""
    # filename
    @show file
    filename = joinpath(abspath,joinpath(splitpath(file)[end-3:end]))
    path = joinpath(abspath,joinpath(splitpath(file)[end-3:end-1]))
    ispath(path) || mkpath(path)
    hdf5fn = filename*fnid*".hdf5"
    isfile(hdf5fn) && rm(hdf5fn)
    # save other relevant quantities
    h5write(hdf5fn,"plaquette",plaquettes(file))
    h5write(hdf5fn,"configurations",confignames(file))
    h5write(hdf5fn,"gauge group",gaugegroup(file))
    h5write(hdf5fn,"beta",couplingβ(file))
    h5write(hdf5fn,"quarkmasses",quarkmasses(file))
    h5write(hdf5fn,"lattice",latticesize(file))
    h5write(hdf5fn,"lofgile",filename)
    h5write(hdf5fn,"measurement_type",type)
    # read correlator data
    c = correlators(file,type;withsource=false,average=true)
    channels = unique(collect(keys(c[1])))
    for Γ in channels
        dat = getindex.(c,Γ)
        h5write(hdf5fn,type*"_"*Γ,reduce(hcat,dat))
    end
end
function writehdf5_spectrum_connected(file,typeU,typeD,typeUD;abspath="")
    fnid = ""
    # filename
    filename = joinpath(abspath,joinpath(splitpath(file)[end-3:end]))
    path = joinpath(abspath,joinpath(splitpath(file)[end-3:end-1]))
    ispath(path) || mkpath(path)
    hdf5fn = filename*fnid*".hdf5"
    isfile(hdf5fn) && rm(hdf5fn)
    # save other relevant quantities
    h5write(hdf5fn,"plaquette",plaquettes(file))
    h5write(hdf5fn,"configurations",confignames(file))
    h5write(hdf5fn,"gauge group",gaugegroup(file))
    h5write(hdf5fn,"beta",couplingβ(file))
    h5write(hdf5fn,"quarkmasses",quarkmasses(file))
    h5write(hdf5fn,"lattice",latticesize(file))
    h5write(hdf5fn,"lofgile",filename)
    h5write(hdf5fn,"measurement_type",type)
    # read correlator data
    for type in (typeU,typeD,typeUD)
        c = correlators(file,type;withsource=false,average=true)
        channels = unique(collect(keys(c[1])))
        for Γ in channels
            dat = getindex.(c,Γ)
            h5write(hdf5fn,type*"_"*Γ,reduce(hcat,dat))
        end
    end
end
function writehdf5_spectrum_disconnected_nondeg(file,type,nhits,masses;filterkey=false,key_pattern="",fnid="",corronly=false,abspath="",only_channels=nothing)
    @show file
    # filename
    filename = joinpath(abspath,joinpath(splitpath(file)[end-3:end]))
    path = joinpath(abspath,joinpath(splitpath(file)[end-3:end-1]))
    ispath(path) || mkpath(path)
    hdf5fn = filename*fnid*".hdf5"
    isfile(hdf5fn) && rm(hdf5fn)
    # save other relevant quantities
    if !corronly
        h5write(hdf5fn,"plaquette",plaquettes(file))
        h5write(hdf5fn,"configurations",confignames(file))
        h5write(hdf5fn,"gauge group",gaugegroup(file))
        h5write(hdf5fn,"beta",couplingβ(file))
        h5write(hdf5fn,"quarkmasses",quarkmasses(file))
        h5write(hdf5fn,"lattice",latticesize(file))
        h5write(hdf5fn,"lofgile",filename)
        h5write(hdf5fn,"sources",nhits)
    end
    # read correlator data
    c1 = correlators(file,type;withsource=true,average=false,masses=true,mass=masses[1],filterkey,key_pattern)
    c2 = correlators(file,type;withsource=true,average=false,masses=true,mass=masses[2],filterkey,key_pattern)
    if isnothing(only_channels)
        channels = unique(collect(keys(c1[1])))
    else
        channels = intersect(unique(collect(keys(c1[1]))),only_channels)
    end
    for Γ in channels
        d1 = getindex.(c1,Γ)
        d2 = getindex.(c2,Γ)
        dat1 = flatten_disc(d1,nhits;rescale=1)
        dat2 = flatten_disc(d2,nhits;rescale=1)
        h5write(hdf5fn,type*"_"*Γ*"_"*masses[1],dat1)
        h5write(hdf5fn,type*"_"*Γ*"_"*masses[2],dat2)
    end
end
function mergehdf_discon(file,type,file1,file2)
    isfile(file) && rm(file)
    hdf5_f1 = h5open(file1, "r")
    hdf5_f2 = h5open(file2, "r")
    # test that we used the same sources in both cases
    @assert keys(hdf5_f1) == keys(hdf5_f2)     
    disc_measurements = filter( x -> contains(x,type), keys(hdf5_f1))
    # test that we compare data from the same lattice setup
    identical = ["beta","gauge group", "lattice",  "plaquette" ,"quarkmasses","configurations"]
    for property in identical
        @assert read(hdf5_f1,property) == read(hdf5_f2,property) "$property mismatch"
        h5write(file,property,read(hdf5_f1,property))
    end
    # loop over all measurements and merge
    N1 = size(read(hdf5_f1,disc_measurements[1]))[1]
    N2 = size(read(hdf5_f2,disc_measurements[1]))[1]
    if N1 != N2 
        @warn "mismatch file1 N=$N1 and file N=$N2"
    end
    N = min(N1,N2)
    for Γ in disc_measurements
        dat = cat(read(hdf5_f1,Γ)[1:N,:,:],read(hdf5_f2,Γ)[1:N,:,:],dims=2)
        h5write(file,Γ,dat)
    end
    h5write(file,"lofgile",file1*file1)
    h5write(file,"sources",read(hdf5_f1,"sources")+read(hdf5_f2,"sources"))
end
function mergehdf_discon(file,type,file1)
    isfile(file) && rm(file)
    cp(file1,file;force=true)
end