using DelimitedFiles
using Plots
using LaTeXStrings
pgfplotsx()

name = "data_nov.csv"
plotdir = split(name,".")[1]
data,header = readdlm(name, '\t', Float64, '\n' ; header=true)

isdir(plotdir) || mkdir(plotdir)
pgfplotsx(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=20,ms=6,framestyle=:box,marker=:auto,legend=:outerright)

# reference scale (from Bennett et.al.)
c1,s1,r1 = (1:10,0.5625,0.692/0.5625)
c2,s2,r2 = (11:20,0.4376,0.5517/0.4376)
c3,s3,r3 = (21:28,0.3702,0.4664/0.3702)
c4,s4,r4 = (30:39,0.3867,0.546/0.3867)
c5,s5,r5 = (40:46,0.3311,0.470/0.3311)
c6,s6,r6 = (47:52,0.2874,0.4019/0.2874)
c7,s7,r7 = (53:61,0.7403,0.8475/0.7403)
cs = (c1,c2,c3,c4,c5,c6,c7)
ss = (s1,s2,s3,s4,s5,s6,s7)
rs = (r1,r2,r3,r4,r5,r6,r7)

for i in 1:length(cs)
    c,s,r = cs[i],ss[i],rs[i]

    β = data[first(c),3]
    βlabel = replace(string(β),"."=>"")
    rlabel = string(round(r,digits=2))
    ensemble = L"~ (β=%$β, ~ m(\rho^d \!\,^e \!\,^g) / m(\pi^d \!\,^e \!\,^g) =%$(round(r,digits=2)) )"
    ratio = L"~( m(\rho^d \!\,^e \!\,^g) / m(\pi^d \!\,^e \!\,^g)=%$rlabel )"

    mπ,  Δmπ  = data[:,6],  data[:,7]
    mπ0, Δmπ0 = data[:,8],  data[:,9]
    mρ,  Δmρ  = data[:,10], data[:,11]
    mρ0, Δmρ0 = data[:,12], data[:,13]
    fπ,  Δfπ  = data[:,14], data[:,15]
    fπ0, Δfπ0 = data[:,16], data[:,17]
    fρ,  Δfρ  = data[:,18], data[:,19]
    fρ0, Δfρ0 = data[:,20], data[:,21]


    δmπ = @. mπ[c] - mπ0[c]
    δmρ = @. mρ[c] - mρ0[c]
    δfπ = @. fπ[c] - fπ0[c]
    δfρ = @. fρ[c] - fρ0[c]
    Δδmπ = @. sqrt.(Δmπ[c]^2 + Δmπ0[c]^2)
    Δδmρ = @. sqrt.(Δmρ[c]^2 + Δmρ0[c]^2)
    Δδfπ = @. sqrt.(Δfπ[c]^2 + Δfπ0[c]^2)
    Δδfρ = @. sqrt.(Δfρ[c]^2 + Δfρ0[c]^2)
    x, Δx = δmπ, Δδmπ

    plt1 = plot(xlabel=L"$m(\pi^A)-m(\pi^C)$ [lattice units]",title = "Difference in masses and decay constants $ensemble")
    plt2 = plot(xlabel=L"$m(\pi^A)-m(\pi^C)$ [lattice units]",title = "Relative difference in masses and decay constants $ensemble")
    scatter!(plt1,x,δmπ,xerr=Δx,yerr=Δδmπ,label=L"m(\pi^A)-m(\pi^C)")
    scatter!(plt1,x,δmρ,xerr=Δx,yerr=Δδmρ,label=L"m(\rho^A)-m(\rho^C)")
    scatter!(plt1,x,δfπ,xerr=Δx,yerr=Δδfπ,label=L"f(\pi^A)-f(\pi^C)")
    scatter!(plt1,x,δfρ,xerr=Δx,yerr=Δδfρ,label=L"f(\rho^A)-f(\rho^C)")

    δmπ = @. (mπ[c] - mπ0[c])/mπ[c][end]
    δmρ = @. (mρ[c] - mρ0[c])/mρ[c][end]
    δfπ = @. (fπ[c] - fπ0[c])/fπ[c][end]
    δfρ = @. (fρ[c] - fρ0[c])/fρ[c][end]
    Δδmπ = @. sqrt.(Δmπ[c]^2 + Δmπ0[c]^2)/mπ[c][end]
    Δδmρ = @. sqrt.(Δmρ[c]^2 + Δmρ0[c]^2)/mρ[c][end]
    Δδfπ = @. sqrt.(Δfπ[c]^2 + Δfπ0[c]^2)/fπ[c][end]
    Δδfρ = @. sqrt.(Δfρ[c]^2 + Δfρ0[c]^2)/fρ[c][end]

    scatter!(plt2,x,δmπ,xerr=Δx,yerr=Δδmπ,label=L"[m(\pi^A)-m(\pi^C)]/m(\pi)^d \!\,^e \!\,^g")
    scatter!(plt2,x,δmρ,xerr=Δx,yerr=Δδmρ,label=L"[m(\rho^A)-m(\rho^C)]/m(\rho)^d \!\,^e \!\,^g")
    scatter!(plt2,x,δfπ,xerr=Δx,yerr=Δδfπ,label=L"[f(\pi^A)-f(\pi^C)]/f(\pi)^d \!\,^e \!\,^g")
    scatter!(plt2,x,δfρ,xerr=Δx,yerr=Δδfρ,label=L"[f(\rho^A)-f(\rho^C)]/f(\rho)^d \!\,^e \!\,^g")

    savefig(plt1,joinpath(plotdir,"deltas_abs_b$(βlabel)_r$rlabel.pdf"))
    savefig(plt2,joinpath(plotdir,"deltas_rel_b$(βlabel)_r$rlabel.pdf"))
end
