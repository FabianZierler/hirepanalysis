using HiRepAnalysis
using Statistics
using Plots

path  = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSU2/Lt32Ls16beta2.2m1-0.75m2-0.75/out"
file  = joinpath(path,"out_spectrumdisc_128_hits")
type  = "DISCON_SEMWALL SINGLET"

function flatten_disc(d,nhits;rescale=1)
    n = div(length(d),nhits)
    T = length(d[1])
    m = zeros(n, nhits, T)
    @inbounds for i in 1:n, j in 1:nhits, t in 1:T
        m[i,j,t] = d[(i-1)*nhits + j][t]
    end
    @. m = rescale*m
    return m
end
function hit_time_average_disconnected(m;rescale=1)
    # (1) average over different hits
    # (2) average over all time separations
    # (3) normalize wrt. time and hit average
    nconf, nhits, T = size(m)
    timavg = zeros(nconf,T)
    norm   = T*div(nhits,2)^2
    hitsd2 = div(nhits,2)
    @showprogress for t in 1:T
        for t0 in 1:T
            Δt = mod(t-t0,T)
            for hit1 in 1:hitsd2, hit2 in hitsd2+1:nhits
                @inbounds for conf in 1:nconf
                    timavg[conf,Δt+1] += m[conf,hit1,t]*m[conf,hit2,t0]
                end
            end
        end
    end
    @. timavg = rescale*timavg/norm
    return timavg
end

T, L = latticesize(file)[1:2]
c = correlators(file,type;withsource=true,average=false)
d = getindex.(c,"g5_disc_re")
m = flatten_disc(d,128;rescale=L^6/2) 
# TODO: think about that factor again:
n = 1/L^3
t = hit_time_average_disconnected(m,rescale=n)
nconf = size(t)[1]
τ = autocorrelation_time(plaquettes(file))
C = mean(t,dims=1)[1,:]
ΔC = sqrt.(var(t,dims=1)[1,:]*τ/nconf)
scatter(C,yerr=ΔC)

