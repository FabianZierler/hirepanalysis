using DelimitedFiles
using Plots
using LaTeXStrings
using LsqFit
pgfplotsx(marker=:auto,legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=20,ms=6,framestyle=:box,legend=:outerright)
ls(s)=latexstring(s)

name = "data_nov.csv"
plotdir = split(name,".")[1]
data,header = readdlm(name, '\t', Float64, '\n' ; header=true)

c1 = 1:10
c2 = 11:20
c3 = 21:28
c4 = 29:33

# reference (f⋅m)^2
s1π, s1ρ = (0.5625*0.1052)^2,(0.692*0.19170)^2
s2π, s2ρ = (0.4376*0.0822)^2,(0.5517*0.1445)^2
s3π, s3ρ = (0.3702*0.0666)^2,(0.4664*0.1121)^2
s4π, s4ρ = (0.2874*0.0575)^2,(0.4019*0.1040)^2

sπ, sρ, s = s1π, s1ρ, 0.5625
c = c1

m1,  m2   = data[c,4],  data[c,5]
mπ,  Δmπ  = data[c,6],  data[c,7]
mπ0, Δmπ0 = data[c,8],  data[c,9]
mρ,  Δmρ  = data[c,10], data[c,11]
mρ0, Δmρ0 = data[c,12], data[c,13]
fπ,  Δfπ  = data[c,14], data[c,15]
fπ0, Δfπ0 = data[c,16], data[c,17]
fρ,  Δfρ  = data[c,18], data[c,19]
fρ0, Δfρ0 = data[c,20], data[c,21]

L = Int(data[last(c),1])
T = Int(data[last(c),2])
β = data[first(c),3]
ensemble = "β=$β"

GMOR(mπ,fπ) = (mπ*fπ)^2
ΔGMOR(m,f,Δm,Δf) = 2m*f*(Δm*f + Δf*m)
G0 = GMOR.(mπ0,fπ0)
G1 = GMOR.(mπ,fπ)
ΔG0 = ΔGMOR.(mπ0,fπ0,Δmπ0,Δfπ0)
ΔG1 = ΔGMOR.(mπ,fπ,Δmπ,Δfπ)

m0 = 0.9304
md = @.(m0 + m2)
mu = m1[1] + m0

gmorLH = @. (mπ*fπ)^2 / sπ
gmor0LH = @. (mπ0*fπ0)^2 / sπ
ΔgmorLH = @. 2*mπ*fπ*sqrt(Δmπ^2*fπ^2 + mπ^2*Δfπ^2) / sπ
Δgmor0LH = @. 2*mπ0*fπ0*sqrt(Δmπ0^2*fπ0^2 + mπ0^2*Δfπ0^2) / sπ

dπ  = (mπ-mπ0) / s
Δdπ = @. sqrt(Δmπ^2 + Δmπ0^2) / s

ylabel = L"\left( m_\pi f_\pi \right)^2 / \left(m_\pi^{deg} f_\pi^{deg} \right)^2"
title = L" %$T \times %$(L)^3: \quad \beta=6.9, \quad \left( \frac{m_\rho}{m_\pi} \right)_{deg} \!\!\!\!\!\! = 1.23"
xlabel = L"pion mass difference $[ m(\pi^A) - m(\pi^C) ] / m(\pi^{deg}) $ "
plt = plot(title=ls(title),xlabel=xlabel,ylabel=ylabel)
scatter!(plt,dπ,gmorLH,xerr=Δdπ,yerr=ΔgmorLH,label=ls(L"\pi^A"))
scatter!(plt,dπ,gmor0LH,xerr=Δdπ,yerr=Δgmor0LH,label=ls(L"\pi^C"))
scatter!(plt,dπ,gmorLH-gmor0LH,xerr=Δdπ,yerr=sqrt.(ΔgmorLH.^2 .+ Δgmor0LH.^2),label=ls(L"\pi^A-\pi^C"))
savefig(plt,joinpath(plotdir,"GMOR.pdf"))

LH = @. GMOR(mπ,fπ) + GMOR(mπ0,fπ0)
ΔLH = @. ΔGMOR(mπ,fπ,Δmπ,Δfπ) + ΔGMOR(mπ0,fπ0,Δmπ0,Δfπ0)
RH_sum(md,mu,Σu,Σd) = 2Σu^2*( (mu+md)/(Σu+Σd) + (Σu*md+Σd*mu)/(Σu^2+Σd^2))
RH_dif(md,mu,Σu,Σd) = 2Σu^2(Σu-Σd)*(Σu*md-Σd*mu)/(Σu+Σd)/(Σu^2+Σd^2)
RH_rat(md,mu,Σu,Σd) = (Σu^2+Σd^2)*(mu+md)/(Σu*md+Σd*mu)/(Σu+Σd)

@. model(x,p) = RH_sum(0.0304,x,p[1],p[2])
f = curve_fit(model,(md.+m0),LH,[1.0,1.0])
t = minimum(md.+m0):0.001:maximum(md.+m0)
scatter((md.+m0),LH,yerr=ΔLH,label="data")
plot!(t,model(t,f.param),label="fit",marker=:none)
