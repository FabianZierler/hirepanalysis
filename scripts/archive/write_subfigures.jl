prmfile = "prm_singlet/prm_plateau.csv"
for i in 2:countlines(prmfile)
    odir, hits, fitη, fitπ, fitσ, fita0, fitρ ,typeM, fileM, fileS = read_prm_combined(prmfile,i)
    T, L = latticesize(fileM)[1:2]
    Nconf, m, G, β = nconfigs(fileS),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
    name=replace(replace("$(G)L$(L)T$(T)b$(β)m$(first(m)).pdf",'('=>""),')'=>"")

    if (fitσ == -1) || (fitσ[2] - fitσ[1] < 3)
        continue
    end
    
    string1 = """ \\begin{figure}[H]
    \t\\centering
    \t\\caption{}
    \t\\includegraphics[width=0.7\\textwidth]{figures_sigma/"""
    string2 = """}
    \t\\label{fig:figures_sigma/"""
    string3 = """}
    \\end{figure}
    """
    
    ioMR = open("subfigure.txt","a+")
    write(ioMR,string1*name*string2*name*string3)
    close(ioMR)
end
for i in 2:countlines(prmfile)
    odir, hits, fitη, fitπ, fitσ, fita0, fitρ ,typeM, fileM, fileS = read_prm_combined(prmfile,i)
    T, L = latticesize(fileM)[1:2]
    Nconf, m, G, β = nconfigs(fileS),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
    name=replace(replace("$(G)L$(L)T$(T)b$(β)m$(first(m)).pdf",'('=>""),')'=>"")
    
    if (fitη == -1) || (fitη[2] - fitη[1] < 3) 
        continue
    end

    string1 = """ \\begin{figure}[H]
    \t\\centering
    \t\\caption{}
    \t\\includegraphics[width=0.7\\textwidth]{figures_eta/"""
    string2 = """}
    \t\\label{fig:figures_eta/"""
    string3 = """}
    \\end{figure}
    """
    
    ioMR = open("subfigure.txt","a+")
    write(ioMR,string1*name*string2*name*string3)
    close(ioMR)
end
#=
list_sigma = String[]
list_eta   = String[]
for i in 2:countlines(prmfile)
    odir, hits, fitη, fitπ, fitσ, fita0, fitρ ,typeM, fileM, fileS = read_prm_combined(prmfile,i)
    T, L = latticesize(fileM)[1:2]
    Nconf, m, G, β = nconfigs(fileS),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
    name=replace(replace("$(G)L$(L)T$(T)b$(β)m$(first(m)).pdf",'('=>""),')'=>"")
    append!(list_sigma,["fig.\\ref{fig:figures_sigma/$name}"])
    append!(list_eta,["fig.\\ref{fig:figures_eta/$name}"])
end

=#