using Plots
using HiRepAnalysis
using DelimitedFiles
using LsqFit
typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"

datapath = "/media/fabian/Archiv/rsyncVSC/runsSp4"
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
datapath = "/media/storage/Fabian_Zierler/runsSp4"

kws1 = (therm=100,step=2,autocor=false)
files1 = [datapath*"/Lt24Ls12beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (50,55,60,65)]
kws2 = (therm=100,step=2,autocor=false)
files2 = [datapath*"/Lt24Ls14beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (70,75,80,85,89,90)]
kws3 = (therm=100,step=2,autocor=false)
files3 = [datapath*"/Lt24Ls12beta7.05m1-0.835m2-0.$(i)/out/out_spectrum" for i in (50,55,60,65)]
kws4 = (therm=100,step=2,autocor=false)
files4 = [datapath*"/Lt24Ls14beta7.05m1-0.835m2-0.$(i)/out/out_spectrum" for i in (70,75,80,82,83)]
kws5 = (therm=100,step=2,autocor=false)
files5 = [datapath*"/Lt24Ls14beta7.2m1-0.78m2-0.$(i)/out/out_spectrum" for i in (50,55,60,65,70,75,77)]

ncut=(8,14)
file=files4[1]
kws=kws2

cor, covV, covM = HiRepAnalysis.average_correlator_ratio(file,"g5",typeU,typeD,typeUD;kws...)
fit1, fit2, fit3, model = fit_corr_bars(cor,covV,ncut)
Δmπ  = fitmass(fit1)
ΔΔmπ = abs(fitmass(fit3)-fitmass(fit2))/2
plt = plot_decay_fit!(plot(),cor,covV;ncut=ncut,label="")
plot!(plt,ylims=extrema(cor))
scatter(cor,yerr=sqrt.(covV))
plt
