using HiRepAnalysis
using Plots
using DelimitedFiles
using LaTeXStrings
pgfplotsx(legendfontsize=18,labelfontsize=16,tickfontsize=14,titlefontsize=16,ms=6,framestyle=:box,marker=:auto,legend=:outerright)

d = readdlm("tables/biagioetal.csv",',',skipstart=1)
mpi_w  = d[:,7]
Δmpi_w = d[:,8]
fpi_w  = d[:,9]
Δfpi_w = d[:,10]
mrho_w = d[:,11]
Δmrho_w= d[:,12]
w0a    = d[:,15]
Δw0a   = d[:,16]
mS_w  = d[:,17]
ΔmS_w = d[:,18]
mAV_w = d[:,19]
ΔmAV_w= d[:,20]

mpi_w  = @. mpi_w*w0a
Δmpi_w = @. Δmpi_w*w0a + mpi_w*Δw0a
mrho_w  = @. mrho_w*w0a
Δmrho_w = @. Δmrho_w*w0a + mrho_w*Δw0a
mS_w  = @. mS_w*w0a
ΔmS_w = @. ΔmS_w*w0a + mS_w*Δw0a
mAV_w  = @. mAV_w*w0a
ΔmAV_w = @. ΔmAV_w*w0a + mAV_w*Δw0a
#=
ratio  = @. mpi_w/fpi_w
Δratio = @. Δmpi_w/fpi_w + Δfpi_w*mpi_w/fpi_w^2 


minratio, maxratio = extrema(ratio)
middle = (minratio + maxratio)/2
width = (minratio - maxratio)/2
chiPT = 5.6 
rectangle(w, h, x, y) = Shape(x .+ [0,w,w,0], y .+ [0,0,h,h])

plt = plot(ylabel=L"m_\pi / f_\pi",xlabel=L"$m_\pi $[GeV] for fixed scale parameter GeV$/\omega_0$")
plot!(plt,xaxis=:log10,xlims=(10^-2,10),ylims=(0,10))
for w in [0.03,0.1,0.4,1,4,10]
    scatter!(plt,mpi_w*w,ratio,xerr=Δmpi_w*w,yerr=Δratio,label=L"GeV/$\omega_0 = %$(w)$")
    #scatter!(mpi_w,mrho_w,xerr=Δmrho_w,yerr=Δratio,label=L"\omega_0 = %$(w)")
end
plot!(plt,[10^-2,10^2],[middle,middle],ribbon=width,ms=0,label="direct lattice data available",la=0)
plot!(plt,[10^-2,10^2],[chiPT/2,chiPT/2],ribbon=chiPT/2,ms=0,label=L"accessible through $\chi$PT",la=0)
plot!(plt,rectangle(0.3,1.2,0.2,5.0), opacity=0.5,ms=0,label="region of interest")
savefig("SIMPlest_miracle_plot_v2.pdf")
=#

#=
plt = scatter(mpi_w,mpi_w./mpi_w,xerr=Δmpi_w,label="pseudoscalar")
scatter!(plt,mpi_w,mrho_w./mpi_w,xerr=Δmpi_w,yerr=Δmrho_w./mpi_w + Δmpi_w.*mrho_w./mpi_w.^2,label="vector")
scatter!(plt,mpi_w,mS_w./mpi_w,xerr=Δmpi_w,yerr=ΔmS_w./mpi_w + Δmpi_w.*mS_w./mpi_w.^2,label="scalar")
scatter!(plt,mpi_w,mAV_w./mpi_w,xerr=Δmpi_w,yerr=Δmrho_w./mpi_w + Δmpi_w.*mrho_w./mpi_w.^2,label="axialvector")
plot!(title  = L"$Sp(4)$ with degenerate fermions - data from 1909.12662")
plot!(ylabel = "Relative non-singlet meson masses")
plot!(xlabel = L"Goldstone mass in lattice units $\omega$")
=#

plt = scatter(mpi_w,mpi_w,xerr=Δmpi_w,yerr=Δmpi_w,label=L"pseudoscalar $\pi$")
scatter!(plt,mpi_w,mrho_w,xerr=Δmpi_w,yerr=Δmrho_w,label=L"vector $\rho$",markershape=:pentagon)
scatter!(plt,mpi_w,mS_w,xerr=Δmpi_w,yerr=ΔmS_w,label=L"scalar $a_0$")
scatter!(plt,mpi_w,mAV_w,xerr=Δmpi_w,yerr=Δmrho_w,label=L"axialvector $a_1$")
plot!(title  = L"$Sp(4)$ with degenerate fermions - data from 1909.12662")
plot!(ylabel = L"Non-singlet meson masses [$\omega$]")
plot!(xlabel = L"Goldstone mass [$\omega$]")
