using HiRepAnalysis
using DelimitedFiles
using Plots
gr(frame=:box)

path = "/home/fabian/Documents/Lattice/HiRepDIaL/measurements/"

file1A = path*"Lt48Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum" 
file1F = path*"Lt48Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum"
file2A = path*"Lt64Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum" 
file2F = path*"Lt64Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum"
file3A = path*"Lt80Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum"
file3F = path*"Lt80Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum" 
file4A = path*"Lt96Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum"
file4F = path*"Lt96Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum" 
type = "DEFAULT_SEMWALL TRIPLET"

plt = plot()

for file in [file1F,file2F,file3F, file4F]
    corrs1 = correlators(file,type,"g1")
    corrs2 = correlators(file,type,"g2")
    corrs3 = correlators(file,type,"g3")

    corrs  = @. (corrs1 + corrs2 + corrs3)

    meff, Δmeff = HiRepAnalysis.implicit_meff_cosh(corrs)

    scatter!(plt, meff, yerr=Δmeff,label="T=$(2*length(meff))")
end

plot!(plt,title="fundamental rho: effective mass")
plot!(plt,ylims=(0.35,0.45))
savefig("fundamental_rho.pdf")

