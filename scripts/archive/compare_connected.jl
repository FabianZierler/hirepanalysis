using HiRepAnalysis
using DelimitedFiles
using Plots
gr(frame=:box)

path = "/home/fabian/Documents/Lattice/HiRepDIaL/measurements/"

file1A = path*"Lt48Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum" 
file1F = path*"Lt48Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum"
file2A = path*"Lt64Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum" 
file2F = path*"Lt64Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum"
file3A = path*"Lt80Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum"
file3F = path*"Lt80Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum" 
file4A = path*"Lt96Ls20beta6.5mf0.71mas1.01AS/out/out_spectrum"
file4F = path*"Lt96Ls20beta6.5mf0.71mas1.01FUN/out/out_spectrum" 

file1 = path*"Lt64Ls32beta6.5mf0.72mas1.01FUN/out/out_spectrum" 

type1 = "DEFAULT_SEMWALL TRIPLET"

plt = plot()
Δratio(x,y,Δx,Δy) = sqrt((Δx / y)^2 + (Δy*x / y^2)^2)
for file1 in [file1F,file2F,file3F, file4F]
    channel = "g1"
    ncut  = 20
    m, Δm = meson_mass_decay_select(file1,channel,type1;error=:jack,nexp2=false,ncut,autocor=false)[1:2]
    meff, Δmeff = HiRepAnalysis.implicit_meff_jackknife(file1,type1,channel)
    meff, Δmeff = HiRepAnalysis.effectivemass_cosh_jackknife(file1,type1,channel;average=true)
    scatter!(meff, yerr=Δmeff,label="T=$(2*length(meff))")
end

plot!(plt,title="fundamental rho: effective mass")
plot!(plt,ylims=(0.35,0.45))
#savefig("~/Downloads/fundamental_rho.pdf")