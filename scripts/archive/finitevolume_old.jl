using HiRepAnalysis
using LsqFit
using Plots
using LaTeXStrings
datapath = "/media/fabian/Archiv/rsyncVSC/runs"
datapath = "/media/storage/Fabian_Zierler/runsSp4"
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"

function finitevolume(L,m,Δm)
    @. model(L,p)  = p[1]*(1+abs(p[2])*exp(-L*p[1])/abs(L*p[1])^(3/2))
    init = [1.0,0.5]
    #@. model(L,p) = p[1] + p[2]*exp(-p[3]*L)/L
    #init = [1.0,0.5,1.0]
    fit  = curve_fit(model,L,m,   init)
    fitp = curve_fit(model,L,m+Δm,init)
    fitm = curve_fit(model,L,m-Δm,init)
    return fit,fitp,fitm, model
end
function finitevolume(L,m,Δm,mGS_inf)
    @. model(L,p)  = p[1]*(1+abs(p[2])*exp(-L*mGS_inf)/abs(L*mGS_inf)^(3/2))
    init = [1.0,0.5]
    fit  = curve_fit(model,L,m,   init)
    fitp = curve_fit(model,L,m+Δm,init)
    fitm = curve_fit(model,L,m-Δm,init)
    return fit,fitp,fitm, model
end
function finitevolume(L,m,Δm,mGS,ΔmGS)
    # best estimate for GS mass
    fit0 = finitevolume(L,mGS,ΔmGS)[1]
    mGS_inf = fit0.param[1]
    # perform fit of other mesons
    return finitevolume(L,m,Δm,mGS_inf)
end
function finitevolumeplot(L,m,Δm;kws...)
    fit, fitp, fitm, model = finitevolume(L,m,Δm)
    return finitevolumeplot(L,m,Δm,fit, fitp, fitm, model;kws...)
end
function finitevolumeplot(L,m,Δm,mGS,ΔmGS;kws...)
    fit, fitp, fitm, model = finitevolume(L,m,Δm,mGS,ΔmGS)
    return finitevolumeplot(L,m,Δm,fit, fitp, fitm, model;kws...)
end
function finitevolumeplot(L,m,Δm,fit, fitp, fitm, model;title="")
    Linv = inv.(L)
    Linv_max = 0.05
    xinv = Linv_max:0.001:1.05/minimum(L)
    # evaluarte model functions for fittet coefs
    x = inv.(xinv)
    y = model(x,coef(fit))
    yp = model(x,coef(fitp))
    ym = model(x,coef(fitm))
    # infinite volume mass
    m∞  = fitmass(fit)
    Δm∞ = max(abs(fitmass(fitp)-fitmass(fit)),abs(fitmass(fitm)-fitmass(fit)))
    # plotting
    ticks = (Linv, @. @. replace(string(rationalize(Linv)),"//"=>"/") )
    plt = plot(xinv,y,label="fit ",ribbon=(abs.(yp-y),abs.(ym-y)),size=(400,435),marker=:none)
    scatter!(plt,Linv,yerr=Δm,m,label="fitted data",marker=:circle)
    scatter!(plt,(0.05,m∞),yerr=Δm∞,label=L"L \rightarrow \infty :  m="*" $(errorstring(m∞,Δm∞))",marker=:square)
    plot!(plt,xlabel="inverse spatial extent 1/L",ylabel="",title=title)
    plot!(plt,xticks=ticks,legend=:topleft)
    return plt
end

masses = [77,83,89]
refmasses = [78,835,90]
βs = ["7.2","7.05","6.9"]
βlabels = ["72", "705", "69"]
pgfplotsx(legendfontsize=18,labelfontsize=22,tickfontsize=14,titlefontsize=24,ms=8,framestyle=:box)

meson_symb=L"\pi"
meson_sup=L"^C" # L"^0"
vector_symb=L"\rho"
meson=meson_symb*meson_sup
vector=vector_symb*meson_sup

for i in 1:length(masses)
    mass = masses[i]
    mass0 = refmasses[i]
    β = βs[i]

    Ls = [8,10,12,14]
    Ts = [16,20,24,24]
    ncuts = [(4,8),(5,10),(6,12),(8,14)]
    therms = (10,10,10,10)
    autocor = (true,true,true,false)
    steps = (1,1,1,2)
    files  = [datapath*"/Lt$(Ts[j])Ls$(Ls[j])beta$(β)m1-0.$(mass0)m2-0.$(mass)/out/out_spectrum" for j in 1:length(Ls) ]
    kws =  [(therm=therms[i],step=steps[i],autocor=autocor[i],ncut=ncuts[i]) for i in 1:4]

    # first use goldstone as reference
    key_GS = "g5"
    ff1 = fit_meff_bars(files[1],typeUD;key=key_GS,kws[1]...)
    ff2 = fit_meff_bars(files[2],typeUD;key=key_GS,kws[2]...)
    ff3 = fit_meff_bars(files[3],typeUD;key=key_GS,kws[3]...)
    ff4 = fit_meff_bars(files[4],typeUD;key=key_GS,kws[4]...)
    uf1 = fit_meff_bars(files[1],typeU,typeD;key=key_GS,kws[1]...)
    uf2 = fit_meff_bars(files[2],typeU,typeD;key=key_GS,kws[2]...)
    uf3 = fit_meff_bars(files[3],typeU,typeD;key=key_GS,kws[3]...)
    uf4 = fit_meff_bars(files[4],typeU,typeD;key=key_GS,kws[4]...)
    mπ  = fitmass.([ff1[1],ff2[1],ff3[1],ff4[1]])
    mπ0 = fitmass.([uf1[1],uf2[1],uf3[1],uf4[1]])
    Δmπ = abs.( fitmass.([ff1[3],ff2[3],ff3[3],ff4[3]]) .- fitmass.([ff1[2],ff2[2],ff3[2],ff4[2]]) )./2
    Δmπ0= abs.( fitmass.([uf1[3],uf2[3],uf3[3],uf4[3]]) .- fitmass.([uf1[2],uf2[2],uf3[2],uf4[2]]) )./2

    key = "g1"
    ff1 = fit_meff_bars(files[1],typeUD;key=key,kws[1]...)
    ff2 = fit_meff_bars(files[2],typeUD;key=key,kws[2]...)
    ff3 = fit_meff_bars(files[3],typeUD;key=key,kws[3]...)
    ff4 = fit_meff_bars(files[4],typeUD;key=key,kws[4]...)
    uf1 = fit_meff_bars(files[1],typeU,typeD;key=key,kws[1]...)
    uf2 = fit_meff_bars(files[2],typeU,typeD;key=key,kws[2]...)
    uf3 = fit_meff_bars(files[3],typeU,typeD;key=key,kws[3]...)
    uf4 = fit_meff_bars(files[4],typeU,typeD;key=key,kws[4]...)
    mM  = fitmass.([ff1[1],ff2[1],ff3[1],ff4[1]])
    mM0 = fitmass.([uf1[1],uf2[1],uf3[1],uf4[1]])
    ΔmM = abs.( fitmass.([ff1[3],ff2[3],ff3[3],ff4[3]]) .- fitmass.([ff1[2],ff2[2],ff3[2],ff4[2]]) )./2
    ΔmM0= abs.( fitmass.([uf1[3],uf2[3],uf3[3],uf4[3]]) .- fitmass.([uf1[2],uf2[2],uf3[2],uf4[2]]) )./2

    plt1 = finitevolumeplot(Ls[1:4],mπ0,Δmπ0,title=meson*" mass [a] at "*L"\beta="*"$β")
    plt2 = finitevolumeplot(Ls[1:4],mM,ΔmM,mπ0,Δmπ0,title=vector*" mass [a] at "*L"\beta="*"$β")

    savefig(plt1,"data_oct/finite_volume_pion_b$(βlabels[i]).pdf")
    savefig(plt2,"data_oct/finite_volume_vector_b$(βlabels[i]).pdf")
end
