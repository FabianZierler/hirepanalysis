using Plots
using HiRepAnalysis
using Statistics
path  = "/home/fabian/Dokumente/rsyncout/rsyncVSC/"
path  = "/home/zierler_fabian/Documents/rsync/rsyncVSC/"
gr()

odir  = path*"runsSp4/Lt24Ls12beta6.9m1-0.90m2-0.90/out/"
fileS = odir*"out_spectrumDisc_v2"
type  = "DISCON_SEMWALL SINGLET"
hits  = 32
Tcut  = 1:12
maxhits = 32
L = latticesize(fileS)[2]
rescale = (L^3)^2 /L^3

function sourcetype(s)
    s == 0 && return "pure volume"
    s == 1 && return "spin + eo"
    s == 2 && return "spin + time"
    s == 3 && return "spin + time + color"
    s == 4 && return "spin + time + color + eo"
    s == 5 && return "spin + color + eo"
end

plt1 = plot()
plt2 = plot()
for s in [0,1,2,3,4,5]
    if s == 1
        c = correlators(fileS,type;withsource=true,average=false)
        d = getindex.(c,"g5_disc_re")
        m = flatten_disc(d,hits;rescale=1)
    else
        fileD = odir*"out_disconnected_s$(s)_h32"
        d = parse_disconnected(fileD)
        m = flatten_disc(d)
    end
    for maxhits in [8]
        if s == 1
            t = hit_time_average_disconnected(m,rescale=rescale,maxhits=maxhits)
        else
            t = hit_time_average_disconnected(m;rescale=1/L^3,maxhits=maxhits)
        end
        C1 = zero(t)
        ΔC = zero(t)
        offset = 0
        # TODO compare to combined mean
        for n in 1+offset:size(t)[1]
            C1[n,:] = mean(t[1+offset:n,:],dims=1)[1,:]
            ΔC[n,:] = sqrt.(var(t[1+offset:n,:],dims=1)[1,:]/(n-offset))
        end
        mΔC = mean(abs.(ΔC[:,Tcut]),dims=2)[1:end]
        plot!(plt1,mΔC,label=sourcetype(s)*", hits=$maxhits",lw=3,markershape=:none)
        plot!(plt2,C1[70,:],label=sourcetype(s)*", hits=$maxhits",markershape=:none)
    end
end
plt2
plt1
plot!(plt1,xlims=(20,200),ylims=(0,0.00035))
plot!(plt1,xlabel="Number of configurations",ylabel="Average std of C(t)_discon.")
plot!(plt1,title="Sp(4): 24 x 12^3 , β = 6.9, m=-0.90, 8 sources ")
plot!(plt1,legendfontsize=8,legend=:bottomleft)
savefig("dilution_comparison_novolume_hit8.svg")
plt1