using HDF5
using Statistics
using Plots
using HiRepAnalysis

file = "/home/zierler_fabian/Downloads/Scattering_6.9e+00_-9.0e-01_-9.0e-01_48_12.hdf5"
file = "/home/zierler_fabian/Downloads/Scattering_6.9e+00_-9.0e-01_-9.0e-01_24_12.hdf5"
file = "/home/zierler_fabian/Downloads/Scattering_6.9e+00_-9.0e-01_-9.0e-01_64_12.hdf5"
hdf5 = h5open(file,"r")
T = read(hdf5,"T")[1]
AD = read(hdf5,"Correlators")[:,:,:,end]
BC = read(hdf5,"Correlators")[:,:,:,end-1]
C  = abs.(2AD  - 2BC)
nhits = 16
D  = reshape(sum(C[:,:,1:nhits],dims=3)/nhits,size(C)[1:2])
C  = reshape(mean(D,dims=2),T)
ΔC = reshape(std(D,dims=2),T)./sqrt(size(D)[2])

#meff, Δmeff = effectivemass(C,ΔC)
#scatter(meff[2:div(T,2)-1],yerr=Δmeff[2:div(T,2)-1],label="T=$T (log)")
meff  = effectivemass_cosh(C)
Δmeff = effectivemass_cosh_err(C,ΔC)
scatter(meff[2:div(T,2)-1],yerr=Δmeff[2:div(T,2)-1],label="T=$T (acosh)")
meff, Δmeff = implicit_meff(C,ΔC)
scatter!(meff[2:div(T,2)-1],yerr=Δmeff[2:div(T,2)-1],label="T=$T (implicit)")
plot!(ylims=(0.5,1.5))


Ct  = zero(C) 
ΔCt = zero(C) 
for i in 1:length(C)
    Ct[i]  = ( C[mod1(i-1,T)] -  C[mod1(i+1,T)])/2 
    ΔCt[i] = (ΔC[mod1(i-1,T)] + ΔC[mod1(i+1,T)])/2
end
meff, Δmeff = implicit_meff(Ct,ΔCt,sign=-1)
scatter!(meff,yerr=Δmeff,ylims=(1,1.5),label="T=$T (implicit - derivative)")

#meff, Δmeff = HiRepAnalysis.effectivemass(Ct,ΔCt)
#scatter!(meff[2:div(T,2)-1],yerr=Δmeff[2:div(T,2)-1],label="T=$T (log - derivative)")

savefig("~/Downloads/effective_masses_update.pdf")
