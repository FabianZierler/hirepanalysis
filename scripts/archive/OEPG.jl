using HiRepAnalysis
using Plots
using LaTeXStrings
typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"
key1 = "g5"
key2 = "g1"

datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
datapath = "/home/zierler_fabian/Documents/rsync/rsyncVSC/runsSp4"

kws = (therm=100,step=1,autocor=true,ncut=4,nexp2=false,error=:bars)

kws = (therm=100,step=2,autocor=false,ncut=6,nexp2=false,error=:bars)
files1 = [datapath*"/Lt24Ls12beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (50,55,60,65)]
files2 = [datapath*"/Lt24Ls14beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (70,75,80,85,89,90)]

files1 = [datapath*"/Lt16Ls8beta6.9m1-0.90m2-0.$i/out/out_spectrum" for i in (45,50,55,60,70)]
files2 = [datapath*"/Lt24Ls12beta6.9m1-0.90m2-0.$(i)/out/out_spectrum" for i in (75,80,85,89,90)]

files = vcat(files1,files2)
nfiles = length(files)

mπ0,Δmπ0,mπ,Δmπ = zeros(nfiles),zeros(nfiles),zeros(nfiles),zeros(nfiles)
mρ0,Δmρ0,mρ,Δmρ = zeros(nfiles),zeros(nfiles),zeros(nfiles),zeros(nfiles)
for i in eachindex(files)
    mπ0[i],Δmπ0[i] = meson_mass_decay_select(files[i],key1,typeD,typeU;kws...)[1:2]
    mπ[i] ,Δmπ[i]  = meson_mass_decay_select(files[i],key1,typeUD;kws...)[1:2]
    mρ0[i],Δmρ0[i] = meson_mass_decay_select(files[i],key2,typeD,typeU;kws...)[1:2]
    mρ[i] ,Δmρ[i]  = meson_mass_decay_select(files[i],key2,typeUD;kws...)[1:2]
end

ref = 300 # reference scale of lightest meson
s = ref/mπ[end]
mρ, Δmρ, mρ0, Δmρ0 = s*mρ, s*Δmρ, s*mρ0, s*Δmρ0
mπ, Δmπ, mπ0, Δmπ0 = s*mπ, s*Δmπ, s*mπ0, s*Δmπ0

x,Δx = mρ, Δmρ

deg_x = mρ[end]
red_y = 420
red_x = 520
pos3_x = 437

blue_x = 340
blue_y = 500

pgfplotsx(ms=5,tickfontsize=15,legendfontsize=13,titlefontsize=20,labelfontsize=20)
plt = plot(size=(800,450),title="Sp(4) Nf=2 : "*" pseudo-Goldstones' "*L"\Pi"*" and iso-non-singlet vector mesons' "*L"P"*" masses")
scatter!(plt,x,mπ,yerr=Δmπ,xerr=Δx,label="flavoured pseudo-Goldstones "*L"\Pi")
scatter!(plt,x,mπ0,yerr=Δmπ0,xerr=Δx,label="unflavoured pseudo-Goldstone "*L"\Pi")
scatter!(plt,x,mρ,yerr=Δmρ,xerr=Δx,label="flavoured vectors "*L"P")
scatter!(plt,x,mρ0,yerr=Δmρ0,xerr=Δx,label="unflavoured vectors "*L"P")
plot!(plt,legend=:bottomright, xlabel="flavoured vector mass [MeV]", ylabel="meson mass [MeV]")

# annotations
annotate!((420,      680,Plots.text("lightest degenerate meson mass set to 300 MeV",:left)))
annotate!((480,      650,Plots.text("PRELIMINARY",:left,:red)))
annotate!((deg_x+5,  562,Plots.text("unflavoured pseudo-Goldstone is",:left)))
annotate!((deg_x+5,  532,Plots.text("the lightest bound state",:left)))
annotate!((pos3_x-10,300,Plots.text("than heavy pseudo-Goldstones",:left)))
annotate!((pos3_x-10,325,Plots.text("light vectors get lighter",:left)))
annotate!((deg_x,    620,Plots.text("degenerate dark quark masses",:left)))

# arrows and lines
# flavoured vectors
posem4 = mρ[5]-10
posem5 = mρ[6]-10
posem6 = mρ[7]-10
posem7 = mρ[8]-10
# unflavoured vectors
posem40 = mρ0[5]-10
posem50 = mρ0[6]-10
# unflavoured vectors
posem4π0 = mπ0[end-3]-10
posem5π0 = mπ0[end-4]-10

vline!([deg_x],linestyle=:dash,color=:black,label="")
plot!([red_y,posem7],[red_x,posem4π0],arrow=true,label="",color=:red,lw=4)
plot!([red_y,posem6],[red_x,posem5π0],arrow=true,label="",color=:red,lw=4)
plot!([blue_y,posem5],[blue_x,posem50],arrow=true,label="",color=:blue,lw=1.5)
plot!([blue_y,posem4],[blue_x,posem40],arrow=true,label="",color=:blue,lw=1.5)

#savefig("posterplot_neu.svg")
