#Pkg.activate(".")
using DelimitedFiles
using Plots
using LaTeXStrings

name = "data_oepg_2.csv"
data,header = readdlm(name, '\t', Float64, '\n' ; header=true)

L = Int(data[1,1])
T = Int(data[1,2])
β = data[1,3]
ensemble = "β=$β"

mπ = data[:,6]
Δmπ = data[:,7]
mπ0 = data[:,8]
Δmπ0 = data[:,9]
mρ = data[:,10]
Δmρ = data[:,11]
mρ0 = data[:,12]
Δmρ0 = data[:,13]
fπ = data[:,14]
Δfπ = data[:,15]
fπ0 = data[:,16]
Δfπ0 = data[:,17]
fρ = data[:,18]
Δfρ = data[:,19]
fρ0 = data[:,20]
Δfρ0 = data[:,21]

ref = 300 # reference scale of lightest meson
s = ref/mπ[end]
mρ, Δmρ, mρ0, Δmρ0 = s*mρ, s*Δmρ, s*mρ0, s*Δmρ0
mπ, Δmπ, mπ0, Δmπ0 = s*mπ, s*Δmπ, s*mπ0, s*Δmπ0

fρ, Δfρ, fρ0, Δfρ0 = s*fρ, s*Δfρ, s*fρ0, s*Δfρ0
fπ, Δfπ, fπ0, Δfπ0 = s*fπ, s*Δfπ, s*fπ0, s*Δfπ0

x,Δx = mρ, Δmρ

deg_x = mρ[end]
red_y = 420
red_x = 520
pos3_x = 437

blue_x = 340
blue_y = 500

pgfplotsx(ms=5,tickfontsize=15,legendfontsize=13,titlefontsize=20,labelfontsize=20)

#masses
#plt = plot(size=(800,450),title="Sp(4) Nf=2 : "*" pseudo-Goldstones' "*L"\Pi"*" and iso-non-singlet vector mesons' "*L"P"*" masses")
#scatter!(plt,x,mπ,yerr=Δmπ,xerr=Δx,label="flavoured pseudo-Goldstones "*L"\Pi")
#scatter!(plt,x,mπ0,yerr=Δmπ0,xerr=Δx,label="unflavoured pseudo-Goldstone "*L"\Pi")
#scatter!(plt,x,mρ,yerr=Δmρ,xerr=Δx,label="flavoured vectors "*L"P")
#scatter!(plt,x,mρ0,yerr=Δmρ0,xerr=Δx,label="unflavoured vectors "*L"P")
#plot!(plt,legend=:bottomright, xlabel="flavoured vector mass [MeV]", ylabel="meson mass [MeV]")

plt = plot(size=(800,450),title="Sp(4) Nf=2 : "*" "*L"\Pi"*" and "*L"P"*" decay constants")
scatter!(plt,x,fπ,yerr=Δfπ,xerr=Δx,label="flavoured pseudo-Goldstones "*L"\Pi")
scatter!(plt,x,fπ0,yerr=Δfπ0,xerr=Δx,label="unflavoured pseudo-Goldstone "*L"\Pi")
scatter!(plt,x,fρ,yerr=Δfρ,xerr=Δx,label="flavoured vectors "*L"P")
scatter!(plt,x,fρ0,yerr=Δfρ0,xerr=Δx,label="unflavoured vectors "*L"P")
plot!(plt,legend=:topleft, xlabel="flavoured vector mass [MeV]", ylabel="meson mass [MeV]")
annotate!((500,155,Plots.text("lightest degenerate meson mass set to 300 MeV",:left)))
annotate!((500,148,Plots.text("PRELIMINARY",:left,:red,:bold)))
ylims!((50,160))

# annotations
#annotate!((420,      680,Plots.text("lightest degenerate meson mass set to 300 MeV",:left)))
#annotate!((480,      650,Plots.text("PRELIMINARY",:left,:red)))
#annotate!((deg_x+5,  562,Plots.text("unflavoured pseudo-Goldstone is",:left)))
#annotate!((deg_x+5,  532,Plots.text("the lightest bound state",:left)))
#annotate!((pos3_x-10,300,Plots.text("than heavy pseudo-Goldstones",:left)))
#annotate!((pos3_x-10,325,Plots.text("light vectors get lighter",:left)))
#annotate!((deg_x,    620,Plots.text("degenerate dark quark masses",:left)))

# arrows and lines
# flavoured vectors
posem4 = mρ[5]-10
posem5 = mρ[6]-10
posem6 = mρ[7]-10
posem7 = mρ[8]-10
# unflavoured vectors
posem40 = mρ0[5]-10
posem50 = mρ0[6]-10
# unflavoured vectors
posem4π0 = mπ0[end-3]-10
posem5π0 = mπ0[end-4]-10

vline!([deg_x],linestyle=:dash,color=:black,label="")
#plot!([red_y,posem7],[red_x,posem4π0],arrow=true,label="",color=:red,lw=4)
#plot!([red_y,posem6],[red_x,posem5π0],arrow=true,label="",color=:red,lw=4)
#plot!([blue_y,posem5],[blue_x,posem50],arrow=true,label="",color=:blue,lw=1.5)
#plot!([blue_y,posem4],[blue_x,posem40],arrow=true,label="",color=:blue,lw=1.5)
savefig("oepg_fpi_frh0.svg")
