using HiRepAnalysis
using BenchmarkTools
using LinearAlgebra

typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"
datapath = "/home/fabian/Dokumente/rsyncout/rsyncVSC/runsSp4"
file = datapath*"/Lt24Ls14beta6.9m1-0.90m2-0.89/out/out_spectrum_mixed"

c, var, cov = HiRepAnalysis.average_correlators(file,typeUD)
