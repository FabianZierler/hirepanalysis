using Plots
using HiRepAnalysis
using LinearAlgebra
using LaTeXStrings
pgfplotsx(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=20,ms=4,framestyle=:box,legend=:outerright)
pgfplotsx(legendfontsize=14,labelfontsize=20,tickfontsize=14,titlefontsize=16,ms=6,framestyle=:box,legend=:topright,size=(400,400))

path  = "/home/fabian/Dokumente/rsyncout/rsyncVSC/"
path  = "/home/zierler_fabian/Documents/rsync/rsyncVSC/"

odir  = path*"runsSp4/Lt32Ls16beta6.9m1-0.90m2-0.90/out/"
fitint, hits = (5,9), 128

odir  = path*"runsSU3/Lt16Ls8beta4.8m1-1.291m2-1.291/out/"
hits, fitint, cut = 128, (3,5), 4

odir  = path*"runsSp4/Lt24Ls12beta6.9m1-0.90m2-0.90/out/"
fitint, hits = (5,9), 64

odir  = path*"runsSp4/Lt24Ls14beta6.9m1-0.90m2-0.90/out/"
fitint, hits = (5,9), 128

odir  = path*"runsSU3/Lt16Ls8beta4.8m1-1.332m2-1.332/out/"
hits, fitint, cut = 128, (3,6), 5

type  = "DISCON_SEMWALL SINGLET"
typeM = "DEFAULT_SEMWALL TRIPLET"
fileS = odir*"out_spectrum_discon"
fileS = odir*"out_spectrum_node_h256"
hits = 256
fileM = odir*"out_spectrum"

channel, state, spin = "g5", L"\eta'" , "ps"
channel, state, spin = "id", L"\sigma", "scalar"

T = latticesize(fileM)[1]
L = latticesize(fileM)[2]
rescaleD = (L^3)^2 /L^3
rescaleC = (L^3)   / 2
key = "$(channel)_disc_re"
key_conn = "$(channel)"

# disconnected contributions from code in /Spectrum/
#Cdisc, ΔCdisc = disconnected_eta(fileS,type,hits;rescale=rescaleD,key=key,therm=2)
#C, ΔC         = disconnected_eta(fileS,type,hits;rescale,vsub=true,key="$(key)_disc_re",therm=kws.therm)

# disconnected pieces
c = correlators(fileS,type;withsource=true)
m = flatten_disc(getindex.(c[2:end],key),hits)

n = size(m)[1]
disc = hit_time_average_disconnected(m;rescale=rescaleD)
conn = getindex.(correlators(fileM,typeM)[2:end],key_conn)*rescaleC
conn_mean = mean(conn)

# ground state correlator
kws = (ncut=cut,error=:hist,nexp2=false)
CI, ΔCI = groundstate_correlator(fileM,key_conn,typeM;kws...)

full    = zero(disc)
fullIm  = zero(disc)
shifted = zero(disc)
shiftedIm = zero(disc)
for i in 1:n
    for j in 1:T
        #full[i,j]   = conn_mean[j] - 2disc[i,j]
        #fullIm[i,j] = CI[j] - 2disc[i,j]
        full[i,j]   = 2disc[i,j]
        fullIm[i,j] = 2disc[i,j]
    end
end
for i in 1:n
    for j in 1:T
        shifted[i,j]   = (full[i,mod1(j-1,T)] - full[i,mod1(j+1,T)])
        shiftedIm[i,j] = (fullIm[i,mod1(j-1,T)] - fullIm[i,mod1(j+1,T)])
    end
end

# This works for g5
τ  = max(1.0,autocorrelation_time(plaquettes(fileS)))
C  = dropdims(mean(full,dims=1),dims=1)
ΔC = dropdims(std(full,dims=1),dims=1)/sqrt(n/τ)

# shifted correlator
C2  = dropdims(mean(shifted,dims=1),dims=1)
ΔC2 = dropdims(std(shifted,dims=1),dims=1)/sqrt(n/τ)

# full improved correlator
C3  = dropdims(mean(fullIm,dims=1),dims=1)
ΔC3 = dropdims(std(fullIm,dims=1),dims=1)/sqrt(n/τ)

# shifted + improved correlator
C4  = dropdims(mean(shiftedIm,dims=1),dims=1)
ΔC4 = dropdims(std(shiftedIm,dims=1),dims=1)/sqrt(n/τ)

m1, Δm1, f1, Δf1 = decay_mass_histogram(C2,diagm(ΔC2.^2),fitint;sign=-1,nexp2=false)
m2, Δm2, f2, Δf2 = decay_mass_histogram(C4,diagm(ΔC4.^2),fitint;sign=-1,nexp2=false)

kws = (ncut=5,error=:hist,nexp2=false,therm=2)
mπ, Δmπ, fπ, Δfπ = meson_mass_decay(fileM,"g5",typeM;kws...)[1:4]
mρ, Δmρ, fρ, Δfρ = meson_mass_decay(fileM,"g1",typeM;kws...)[1:4]

# prepare titles for the plots
m = quarkmasses(fileM)
G = gaugegroup(fileM)
β = couplingβ(fileM)
title=L"$ %$(T)\times %$(L)^3, \beta=%$β, m_q=%$(first(m))$"
title=L"$ %$(T)\times %$(L)^3, \beta=%$β, %$G, m_q=%$(first(m)),%$(last(m))$"

sπ = errorstring(mπ,Δmπ)
sη = errorstring(m2,Δm2)
sρ = errorstring(mρ,Δmρ)

t = 1:div(T,2)
meff1, Δmeff1 = implicit_meff(C4,ΔC4;sign=-1)
meff2, Δmeff2 = implicit_meff(C2,ΔC2;sign=-1)
cut = 1:div(T,2)

# find plot y-limits
lim1 = minimum((mπ-Δmπ, mρ-Δmρ, mη-Δmη))
lim2 = maximum((mπ+Δmπ, mρ+Δmρ, mη+Δmη))

plot(title=title)
scatter!(t[cut], meff1[cut], yerr=Δmeff1[cut],label=L"eff. mass. $%$(state)$ (b)")
plot!(collect(fitint),ones(2)*m2,ribbon=Δm2,label=L"$m(%$(state))=%$sη$")
plot!(1:T,mπ*ones(T),ribbon=Δmπ, label=L"$m(\pi)=%$sπ$",markershape=:none)
plot!(1:T,mρ*ones(T),ribbon=Δmρ,label=L"$m(\rho)=%$sρ$",markershape=:none)
plot!(ylims=(0.9*lim1,1.1*lim2),xlims=(1.5,T/2 +.5),xticks=2:div(T,2))
plot!(xlabel=L"t/a", ylabel=L"a m_{eff}")
savefig("$(spin)_singlet_m1$(m[1])_m1$(m[1])_L$(L)_subtracted(b).pdf")
plot!()