using Plots
using DelimitedFiles
using LaTeXStrings
pgfplotsx(legendfontsize=18,labelfontsize=20,tickfontsize=14,titlefontsize=20,framestyle=:box,legend=:outerright)

d = readdlm("tables/arthur_et_al.csv",'\t',skipstart=1)
replace!(d,"-" => 0.0)

mη  = d[:, 8]./d[:,14]
Δmη = d[:, 9]./d[:,14]
mσ  = d[:,10]./d[:,14]
Δmσ = d[:,11]./d[:,14]

mπ  = d[:,12]./d[:,14]
Δmπ = d[:,13]./d[:,14]
mρ  = d[:,14]./d[:,14]
Δmρ = d[:,15]./d[:,14]

#r  = mπ ./ mρ
#Δr = @. sqrt((Δmπ/mρ)^2 + (Δmρ*mπ/mρ^2)^2)

β1 = 1:6
β2 = 7:12
β2a = 10:12
β2b = 8:8

plot()
scatter!(mπ[β1],xerr=Δmπ[β1],mπ[β1],yerr=Δmπ[β1], color = :lightblue,  markershape=:circle,   label=L"\pi")
#scatter!(mπ[β1],xerr=Δmπ[β1],mρ[β1],yerr=Δmρ[β1], color = :orange,     markershape=:pentagon, label=L"\rho")
#scatter!(mπ[β1],xerr=Δmπ[β1],mη[β1],yerr=Δmη[β1], color = :green,      markershape=:diamond,  label=L"\eta")
scatter!(mπ[β1],xerr=Δmπ[β1],mσ[β1],yerr=Δmσ[β1], color = :magenta,    markershape=:triangle, label=L"\sigma (\beta=2.0)")

scatter!(mπ[β2],xerr=Δmπ[β2],mπ[β2],yerr=Δmπ[β2], color = :lightblue,  markershape=:circle,   label="")
#scatter!(mπ[β2],xerr=Δmπ[β2],mρ[β2],yerr=Δmρ[β2], color = :orange,     markershape=:pentagon, label="")
#scatter!(mπ[β2],xerr=Δmπ[β2],mη[β2],yerr=Δmη[β2], color = :green,      markershape=:diamond,  label="")

scatter!(mπ[β2a],xerr=Δmπ[β2a],mσ[β2a],yerr=Δmσ[β2a], color = :blue,markershape=:triangle, label=L"\sigma (\beta=2.2)")
scatter!(mπ[β2b],xerr=Δmπ[β2b],mσ[β2b],yerr=Δmσ[β2b], color = :blue,markershape=:triangle, label="")

plot!(mπ[β1],xerr=Δmπ[β1],mρ[β1], color = :orange,     markershape=:pentagon, label=L"\rho", ms=0, lw =3)
plot!(mπ[β2],xerr=Δmπ[β2],mρ[β2], color = :orange,     markershape=:pentagon, label="", ms=0, lw =3)

plot!(title=L"$SU(2)$ with $N_f=2$ fund. from 1607.06654 and 1602.06559")
plot!(xlabel=L"m_\pi / m_\rho", ylabel=L"m_H/m_\rho", legend=:topright)
plot!(ylims=(0.5,1.2),legend=:bottomright)
savefig("nf2su2_eta_spacing.pdf")