using HiRepAnalysis
using HDF5
using DelimitedFiles
function writehdf5_spectrum_connected(file,h5file,type)
    # filename
    @show file
    group = joinpath(splitpath(file)[end-3:end-2])
    
    # save other relevant quantities
    h5write(h5file,"$group/plaquette",plaquettes(file))
    h5write(h5file,"$group/configurations",confignames(file))
    h5write(h5file,"$group/gauge group",gaugegroup(file))
    h5write(h5file,"$group/beta",couplingβ(file))
    h5write(h5file,"$group/quarkmasses",quarkmasses(file))
    h5write(h5file,"$group/lattice",latticesize(file))
    h5write(h5file,"$group/lofgile",joinpath(splitpath(file)[end-3:end]))
    h5write(h5file,"$group/measurement_type",type)
    # read correlator data
    c = correlators(file,type;withsource=false,average=false)
    channels = unique(collect(keys(c[1])))
    for Γ in channels
        dat = getindex.(c,Γ)
        h5write(h5file,"$group/$Γ",reduce(hcat,dat))
    end
end

typeALT = "SEMWALL_UD TRIPLET"
type  = "DEFAULT_SEMWALL TRIPLET"
files = readdlm("spectrum_logfiles_list")[:,1]

# degenerate output files
for file in files
    contains(file,"runsSU3") && continue
    writehdf5_spectrum_connected(file,"data_connected.hdf5",type)
end