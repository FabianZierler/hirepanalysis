using DelimitedFiles
data_v0 = readdlm("DatafreezeSinglets/data/data_deriv.csv",';',skipstart=1) 
param = readdlm("DatafreezeSinglets/parameters/param_deriv.csv",';',skipstart=1) 

# data from main tables
name  = param[:,11]
group = param[:,10]
data1 = data_v0[:,1:11] # β;m;L;T;Nconf;Nhits;fitη;fitπ;fit_sigma;fit_a0;fit_rho
P     = data_v0[:,22:23]
mπL   = data_v0[:,26:27]
mπρ   = data_v0[:,24:25]
data2 = data_v0[:,12:15] # mπ;Δmπ;mρ;Δmρ
data3 = data_v0[:,18:21] # mη;Δmη;mσ;Δmσ

# data from appendix
data_v1 = readdlm("DatafreezeSinglets/data/data_vsub.csv",';',skipstart=1)
data_v2 = readdlm("DatafreezeSinglets/data/data_nothing.csv",';',skipstart=1)
data_v3 = readdlm("DatafreezeSinglets/data/data_const.csv",';',skipstart=1)
data_v4 = readdlm("DatafreezeSinglets/data/data_only_deriv.csv",';',skipstart=1) 
data_v5 = readdlm("DatafreezeSinglets/data/data_both.csv",';',skipstart=1) 
data_v6 = readdlm("DatafreezeSinglets/data/data_both_nogs.csv",';',skipstart=1)

mη1 = data_v1[:,18:19]
mη2 = data_v2[:,18:19]
mη3 = data_v3[:,18:19]
mη4 = data_v4[:,18:19]
mσ1 = data_v5[:,20:21]
mσ2 = data_v1[:,20:21]
mσ3 = data_v6[:,20:21]

data = hcat(name,group,data1,P,mπL,mπρ,data2,data3,mη1,mη2,mη3,mη4,mσ1,mσ2,mσ3)
data = replace(data,NaN => "-")
data = replace(data,-1 => "-")
header1 = ["ensemble" "group" "beta" "m_0" "L" "T" "nconf" "nsrc" "I_eta'" "I_pi" "I_sigma" "I_sigma_conn" "I_rho"]
header2 = ["P" "ΔP" "mπ*L" "Δmπ*L" "mπ/mρ" "Δ(mπ/mρ)" "mπ" "Δmπ" "mρ" "Δmρ" "mη'" "Δmη'" "mσ" "Δmσ"  ]
header3 = [ "mη'1" "Δmη'1" "mη'2" "Δmη'2" "mη'3" "Δmη'3" "mη'4" "Δmη'4" "mσ1" "Δmσ1" "mσ2" "Δmσ2" "mσ3" "Δmσ3"] 
header = hcat(header1, header2, header3)
writedlm("tables_degenerate.csv", vcat(header,data), ';')

# NON-DEG PARAMETERS
# Main results for the singlet mesons with degenerate fermions, TABLE II
data_v0  = readdlm("DatafreezeSinglets/data/data_non_deg.csv",';',skipstart=1) 
param_v0 = readdlm("DatafreezeSinglets/parameters/param_non_deg.csv",';',skipstart=1) 

prm  = hcat(param_v0[:,10],data_v0[:,1:15])
p2 = data_v0[:,15:24]
p3 = data_v0[:,27:28]
p4 = data_v0[:,25:26]
data = hcat(prm,p2,p3,p4)
data = replace(data,-1 => "-")
data = replace(data,NaN => "-")
header1 = ["Ensemble" "beta" "m_0^1" "m_0^2" "L" "T" "nconf" "nsrc" "I_eta'" "I_pi^0" "I_pi^+-" "I_sigma" "I_sigma^conn" "I_rho"]
header2 = ["P" "ΔP" "mpi^0" "Δmpi^0" "mpi^0_c" "Δmpi^0_c" "mpi^+-" "Δmpi^+-" "mrho^0" "Δmrho^0" "mrho^+-" "Δmrho^+-" "m_eta'" "Δm_eta'" "m_sigma" "Δm_sigma"]
header = hcat(header1, header2)
writedlm("tables_nondegenerate.csv",vcat(header,data),";")