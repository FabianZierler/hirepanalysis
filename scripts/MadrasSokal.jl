using Statistics
function madras_sokal_fixedt(x, t)
    m = mean(x)
    Γ = zero(eltype(x))
    N = length(x)
    for i in 1:N-t
        Γ += (x[i]-m)*(x[i+t]-m)/(N-t)
    end
    return Γ 
end
function madras_sokal(x,tmax)
    Γ = zeros(eltype(x),tmax)
    for t in 1:tmax
        Γ[t] = madras_sokal_fixedt(x, t)
    end
    return Γ 
end
