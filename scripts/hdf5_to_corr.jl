using HDF5
using HiRepAnalysis
using Statistics
using DelimitedFiles
using Plots
path  = "/home/fabian/Dokumente/HiRep/hdf_files/"
path  = "/media/fabian/HDD#3/hdf_files/"
path  = "/home/zierler_fabian/Documents/rsync/hdf_files/"
corrpath = "/home/zierler_fabian/Documents/rsync/correlators_cosh_vmean/"
corrpath = "/home/zierler_fabian/Documents/rsync/correlators_cosh_pseudoscalars/"

function read_prm_v2(file,i)
    prm = readdlm(file,';')
    odir = prm[i,1]*"/out/"
    typeC = prm[i,6]
    typeD = "DISCON_SEMWALL SINGLET" 
    fileC,fileD = "out_spectrum.hdf5", prm[i,10]
    hits = prm[i,2]
    return odir, typeC, typeD, fileC, fileD, hits
end
prmfile = "prm_eta/prm_all.csv"
for i in 1:countlines(prmfile)
    odir, typeC, typeD, fileC, fileD, hits = read_prm_v2(prmfile,i)

    fileDISC = joinpath(path,odir,fileD)
    fileCONN = joinpath(path,odir,fileC)
    # read existing data from HDF5 file
    @assert HDF5.ishdf5(fileDISC)
    hdf5DISC = h5open(fileDISC, "r")
    @assert HDF5.ishdf5(fileCONN)
    hdf5CONN = h5open(fileCONN, "r")

    L = read(hdf5DISC,"lattice")[2]
    rescale = (L^3)^2 /L^3

    # set-up file for correlator data
    newfile = joinpath(corrpath,splitpath(odir)[2]*".hdf5")
    ispath(corrpath) || mkpath(corrpath)
    isfile(newfile) && rm(newfile)
    # write other information
    for property in ["quarkmasses","gauge group","beta","lattice"]
        h5write(newfile,property,read(hdf5DISC,property))
    end
    # obtain list of all gamma-combinations
    disc_measurements = getindex.(split.(filter( x -> contains(x,typeD), keys(hdf5DISC)),typeD*"_") ,2)
    conn_measurements = getindex.(split.(filter( x -> contains(x,typeC), keys(hdf5CONN)),typeC*"_"),2)
    # filter out imaginary part, equvivalent vector gamma structures and CL 
    filter!(x-> !contains(x,"g2") && !contains(x,"g3") && !contains(x,"im") && !contains(x,"CL") ,disc_measurements)
    filter!(x-> !contains(x,"g2") && !contains(x,"g3") && !contains(x,"im") && !contains(x,"CL") ,conn_measurements)
    # just use g5 and id anyway ^^
    #conn_measurements = ["id", "g5"]
    #disc_measurements = ["id_disc_re", "g5_disc_re"]
    #disc_measurements = ["id_disc_re"]
    #conn_measurements = ["id"]
    disc_measurements = ["g5_disc_re"]
    conn_measurements = ["g5"]
    # obtain correlator of the disconnected pieces with/without vacuum subtraction and with/without derivative
    for (gamma_disc, gamma_con) in zip(disc_measurements,conn_measurements), vsub in [true], deriv in [false]
        # disconnected pieces
        @show gamma_disc, gamma_con
        C_disc_MC = disconnected_eta_MC(fileDISC,typeD,hits;rescale,vsub=vsub,key=gamma_disc)
        C_conn_MC = correlators(fileCONN,typeC,gamma_con)
        HiRepAnalysis._rescale_corrs!(C_conn_MC, L)

        Cd_jk, C_jk, meff_jk, mη_jk = singlet_jackknife(C_conn_MC,C_disc_MC,10,(6,8);deriv,gs_sub=false,sigma=false)
        mη, Δmη = first.(apply_jackknife(mη_jk))
        meffη, Δmeffη = apply_jackknife(meff_jk;ignoreNaN=true)
        C_sing, ΔC_sing   = apply_jackknife(C_jk)
        Cd_sing, ΔCd_sing = apply_jackknife(Cd_jk)

        C_conn = dropdims(mean(C_conn_MC,dims=2),dims=2)
        Delta_C_conn = dropdims(std(C_conn_MC,dims=2),dims=2)./sqrt(size(C_conn_MC)[2])
        plot(Cd_sing, yerr=ΔCd_sing,title=splitpath(odir)[2])
        plot(C_sing,  yerr=ΔC_sing, title=splitpath(odir)[2])
        display(plot!())

        h5write(newfile,gamma_con*"_singlet_corr",C_sing)
        h5write(newfile,gamma_con*"_multiplet_corr",C_conn)
        h5write(newfile,gamma_con*"_singlet_Delta_corr",ΔC_sing)
        h5write(newfile,gamma_con*"_multiplet_Delta_corr",Delta_C_conn)
    end
end