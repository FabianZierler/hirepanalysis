using HiRepAnalysis
using Statistics
using Parsers
path  = "/media/fabian/HDD#3/outfiles/"
path  = "/home/zierler_fabian/Documents/rsync/rsyncVSC/"

function correlator_deriv(C)
    N,T = size(C)
    Cd  = zero(C) 
    for j in 1:N, i in 1:T
        Cd[j,i]  = ( C[j,mod1(i-1,T)] -  C[j,mod1(i+1,T)])/2 
    end
    return Cd
end
function parse_I0_scattering(file)
    # get number of stochastics sources and time extent
    h = nhits(file)
    N = nconfigs(file)
    T = latticesize(file)[1]
    # init variuables for metadata
    src  = 0
    ncfg = 0
    cfg0 = "dummy"
    name = "channel_dummy"
    # Arrays for parsed data 
    pi1  = zeros(N,h,T)
    disc = zeros(N,h,T)
    V = zeros(N,h,T)
    D = zeros(N,h,T)
    C = zeros(N,h,T)
    R = zeros(N,T)
    # star parsing
    offset = length("[IO][0]")
    for line in eachline(file)
        if startswith(line,"[IO][0]")
            if startswith(line,"[IO][0]Configuration")
                continue
            end
            if !isdigit(line[offset+1])
                # parse metadata from outputfile
                posA = first(findfirst("src", line))
                posB = first(findfirst("run", line))
                name = line[offset+1:posA-2]
                src  = Parsers.parse(Int,line[posA+4:posB-2])+1
                cfg  = line[posB+3:end]
                # increment # of configuration if its name changes
                if cfg != cfg0
                    ncfg += 1
                    #@show ncfg
                    cfg0 = cfg
                end
            else
                # if data starts with a digit then this is the numerical data
                # parse it intop appropriate arrays 
                posA = findnext(' ',line,offset+1)
                #px = Parsers.parse(Int,line[offset+1:posA-1])
                posB = findnext(' ',line,posA+1)
                #py = Parsers.parse(Int,line[posA+1:posB-1])
                posA = findnext(' ',line,posB+1)
                #pz = Parsers.parse(Int,line[posB+1:posA-1])
                posB = findnext(' ',line,posA+1)
                t  = Parsers.parse(Int,line[posA+1:posB-1])+1
                posA = findnext(' ',line,posB+1)
                Re = Parsers.parse(Float64,line[posB+1:posA-1])
                #Im = Parsers.parse(Float64,line[posA+1:end])
                # write results int arrays
                (name == "V")    && (V[ncfg,src,t] = Re)
                (name == "D")    && (D[ncfg,src,t] = Re)
                (name == "C")    && (C[ncfg,src,t] = Re)
                (name == "pi1")  && (pi1[ncfg,src,t] = Re)
                (name == "disc") && (disc[ncfg,src,t] = Re)
                (name == "Ralt") && (R[ncfg,t] = Re)
            end
        end
    end
    return V, D, C, pi1, disc, R
end

odir = path*"runsSp4/Lt24Ls12beta6.9m1-0.90m2-0.90/out/"
file = odir*"out_scattering_I0"
V, D, C, pi1, disc, R0 = @time parse_I0_scattering(file);

maxhits = 12
maxconf = 2858
##### Question
# Scale of V
V, D, C = V[1:maxconf,1:maxhits,:], D[1:maxconf,1:maxhits,:], C[1:maxconf,1:maxhits,:]
R0 = R0[1:maxconf,:]

N = size(V)[1]
V0 = hit_time_average_disconnected(V;rescale=1)
#V0 = dropdims(mean(V,dims=2),dims=2)
D0 = dropdims(mean(D,dims=2),dims=2)
C0 = dropdims(mean(C,dims=2),dims=2)
π0 = dropdims(mean(pi1,dims=2),dims=2)
d0 = dropdims(mean(disc,dims=2),dims=2)

Corr0  = 2D0 + 2C0 # - 10R0 + 5V0
Corr0  = 2D0 + 3C0 - 10R0 + 5V0
Corr0_der = correlator_deriv(Corr0)

V = dropdims(mean(V0,dims=1),dims=1)
Corr = dropdims(mean(Corr0,dims=1),dims=1)
Corr_der = dropdims(mean(Corr0_der,dims=1),dims=1)
ΔV = dropdims(std(V0,dims=1),dims=1)/sqrt(N)
ΔCorr = dropdims(std(Corr0,dims=1),dims=1)/sqrt(N)
ΔCorr_der = dropdims(std(Corr0_der,dims=1),dims=1)/sqrt(N)

meff, Δmeff = implicit_meff(Corr_der,ΔCorr_der,sign=-1)

Nconf = nconfigs(file)
G = gaugegroup(file)
β = couplingβ(file)
T = latticesize(file)[1]
L = latticesize(file)[2]
using LaTeXStrings
title=L"$ %$(T)\times %$(L)^3, \beta=%$β, %$G, m_q=-0.90$, $N_{\rm cfg} = %$N$, $n_{\rm hits} = 16$"

using Plots
#scatter(abs.(Corr_der),yerr=ΔCorr_der,yscale=:log10)
#pgfplotsx(legendfontsize=14,labelfontsize=20,tickfontsize=14,titlefontsize=16,ms=4,framestyle=:box,legend=:outerright)
scatter(5:12, meff[5:12],yerr=Δmeff[5:12],ylims=(1,1.5),label=L"$\pi \pi : I=0$")
#scatter!(5:12, meff[5:12],yerr=Δmeff[5:12],ylims=(1,1.5),label=L"$\pi \pi : I=2$")
plot!(xlabel="Euclidean t", ylabel="Effective mass",xticks=5:12,title=title)
plot!([5,12],[2*0.5667,2*0.5667], ribbon=2*0.0007,label=L"2 m_\pi")
plot!([5,12],[1.119,1.119], ribbon=0.002,label=L"m_{\pi \pi} (I=2)")
#savefig("Isospin0_2pi_Correlator.pdf")

