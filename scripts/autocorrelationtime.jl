using HiRepAnalysis
using Plots
plotly()
path = "/media/fabian/HDD#3/rsyncout/rsyncVSC/"
path = "/home/fabian/Dokumente/rsyncout/rsyncVSC/"
path = "/home/zierler_fabian/Documents/rsync/rsyncVSC/"
file = path*"runsSU3/Lt30Ls10beta5.4m1-0.89m2-0.89/out/out_hmc" 

therm = 100
s = 12
p = plaquettes(file)[therm:s:end]
autocorrelation_time(p[1:1:end],correct=false,minlags=100)
scatter(autocorrelation(p,minlags=100))
serieshistogram(p[1:1:end])
plot!(ylims=(.53,.545))