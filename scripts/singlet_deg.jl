using Plots
using HiRepAnalysis
using LinearAlgebra
using LaTeXStrings
using BenchmarkTools
using DelimitedFiles
pgfplotsx(legendfontsize=12,labelfontsize=20,tickfontsize=14,titlefontsize=16,ms=4,framestyle=:box,legend=:outerright)
gr(legendfontsize=12,labelfontsize=20,tickfontsize=20,titlefontsize=18,ms=4,framestyle=:box,legend=:topright,size=(800,500))
plotly(legendfontsize=24,labelfontsize=24,tickfontsize=24,titlefontsize=22,ms=4,framestyle=:box,legend=:outerright,size=(800,500))
pgfplotsx(legendfontsize=18,labelfontsize=24,tickfontsize=24,titlefontsize=22,ms=4,framestyle=:box,legend=:outerright,size=(800,500))
pgfplotsx(legendfontsize=22,labelfontsize=24,tickfontsize=18,titlefontsize=22,ms=4,framestyle=:box,legend=:outerright,size=(800,500))
gr(legendfontsize=12,labelfontsize=20,tickfontsize=20,titlefontsize=14,ms=4,framestyle=:box,legend=:outerright,size=(800,500))

function errorbars_semilog(x,Δx)
    lower = max.(x - Δx,10E-15)
    bars = (Δx,x-lower)
    return bars
end
parse_fit_intervall(s) = isa(s,Integer) ? s : Tuple(parse.(Int,split(s[2:end-1],',')))
function read_prm(file,i)
    prm = readdlm(file,';')
    odir = joinpath(path,prm[i,1],"out")
    hits = prm[i,2]
    fitη,fitπ,fitσ,fita0,fitρ = parse_fit_intervall.(prm[i,3:7])
    fileM, fileS = joinpath(odir,"out_spectrum.hdf5"), joinpath(odir,prm[i,9])
    group,name,vsub,deriv,gs_sub,constant = prm[i,10:15]
    return odir, hits, fitη, fitπ, fitσ, fita0, fitρ ,fileM, fileS, group, name, vsub, deriv, gs_sub, constant
end
function singlet_analysis(fileM,fileS,hits,vsub,key,cut,fitint,deriv,gs_sub,sigma,constant;maxconf=typemax(Int))
    type  = "DISCON_SEMWALL SINGLET" 
    typeM = "DEFAULT_SEMWALL TRIPLET"
    T, L = latticesize(fileM)[1:2]
    rescale = (L^3)^2 /L^3
    # disconnected contributions from code in /Spectrum/
    # perform correct normalization
    C_dis_MC = disconnected_eta_MC(fileS,type,hits;maxhits=hits,rescale,vsub,key="$(key)_disc_re")
    C_con_MC = correlators(fileM,typeM,key)
    HiRepAnalysis._rescale_corrs!(C_con_MC, L)
    # restrict analysis to first N configurations
    if size(C_dis_MC)[1] != size(C_con_MC)[2] 
        @warn "mismatch between connected  $(size(C_con_MC)[2]) and disconnected $(size(C_dis_MC)[1])"
        N = min(size(C_dis_MC)[1],size(C_con_MC)[2],maxconf)
        C_dis_MC = C_dis_MC[1:N,:] 
        C_con_MC = C_con_MC[:,1:N]
    end

    mη, Δmη, Cd,ΔCd, C, ΔC, meffη, Δmeffη, cη, Δcη = singlet_jackknife(C_con_MC,C_dis_MC,cut,fitint;deriv,gs_sub,sigma,constant)
    return mη, Δmη, meffη, Δmeffη, cη, Δcη, C, ΔC, Cd, ΔCd
end
function filter_meff_for_plot!(meffη,Δmeffη,mπ)
    for i in eachindex(Δmeffη)
        if Δmeffη[i] > meffη[i] || Δmeffη[i] > 2mπ  
            meffη[i], Δmeffη[i] = NaN, NaN
        end
        if meffη[i] < 0.01
            meffη[i], Δmeffη[i] = NaN, NaN
        end
    end
end
function singlet_effective_mass_plot(meffη,Δmeffη,mπ,Δmπ,mη,Δmη,mρ,Δmρ,key,fitint,deriv,fileM,hits,state;kws...)
    T, L = latticesize(fileM)[1:2]
    Nconf, m, G, β = nconfigs(fileM),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
    title=L"$ %$(T)\times %$(L)^3, \beta=%$β, %$G, m_q=%$(first(m))$, $N_{cfg} = %$Nconf$, $n_{\rm src} = %$hits$"
    plt = plot(title=title)
    singlet_effective_mass_plot!(plt,meffη,Δmeffη,mπ,Δmπ,mη,Δmη,mρ,Δmρ,key,fitint,deriv,fileM,hits,state;kws...)
end
function singlet_effective_mass_plot!(plt,meffη,Δmeffη,mπ,Δmπ,mη,Δmη,mρ,Δmρ,key,fitint,deriv,fileM,hits,state;save=false,fit=true,connected=true,update_limit=true)
    T, L = latticesize(fileM)[1:2]
    lim1 = 0.5*mπ
    lim2 = 2.0*mρ
    Nconf, m, G, β = nconfigs(fileM),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
    sπ, sη, sρ = errorstring(mπ,Δmπ), errorstring(mη,Δmη), errorstring(mρ,Δmρ)
    savefile=replace(replace("$(G)L$(L)T$(T)b$(β)m$(first(m)).pdf",'('=>""),')'=>"")
    fitη = fitint[1]:fitint[2]
    t    = 1:T÷2
    filter_meff_for_plot!(meffη,Δmeffη,mπ)
    mefflabel = deriv ? latexstring(state*" eff. mass (deriv.)") : latexstring(state*" eff. mass (direct)") 
    masslabel = isequal(key,"g5") ? L"$m_{\eta'}=%$sη$" : isequal(key,"id") ? L"$m_{\sigma}=%$sη$"  : L"$m=%$sη$" 
    if connected 
        plot!(plt,1:T,mπ*ones(T),ribbon=Δmπ, label=L"$m_\pi=%$sπ$",markershape=:none)
        plot!(plt,1:T,mρ*ones(T),ribbon=Δmρ,label=L"$m_\rho=%$sρ$",markershape=:none)
    end
    scatter!(plt,t, meffη[t], yerr=Δmeffη[t], label=mefflabel)
    if fit 
        plot!(plt,fitη,mη*ones(length(fitη)),ribbon=Δmη, label=masslabel,markershape=:none)
    end
    plot!(plt,ylims=(lim1,lim2),xlims=(1.5,T/2 +.5),xticks=2:div(T,2),xlabel=L"t/a", ylabel=L"a m_{\rm eff}")
    if save
        savedir = isequal(key,"g5") ? "figures_eta/" : "figures_sigma/"
        ispath(savedir) || mkdir(savedir)
        savefig(savedir*savefile)
    end
    return plt
end

function write_singlet_files(filedat,fileprm,fileM,fileS,fitη,fitπ,fitσ,fita0,fitρ,mπ,Δmπ,mρ,Δmρ,ma0,Δma0,mη,Δmη,mσ,Δmσ,fileid,hits,typeM,P,ΔP,group,name,vsub,deriv,gs_sub,constant)
    T, L = latticesize(fileM)[1:2]
    Nconf, m, G, β = nconfigs(fileM),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
    #= ioPR = open("DatafreezeSinglets/parameters/"*fileprm*".csv","a+")
    iszero(position(ioPR)) && write(ioPR,"file;hits;fitη;fitπ;fit_sigma;fit_a0;fit_rho;Nconf;hdf5file;group;name;vsub;deriv;gs_sub;constant;\n")
    write(ioPR,"$fileid;$hits;$fitη;$fitπ;$fitσ;$fita0;$fitρ;$Nconf;$(splitpath(fileS)[end]);$group;$name;$vsub;$deriv;$gs_sub;$constant\n")
    close(ioPR) =#
    # calculate ratios 
    Δratio(x,y,Δx,Δy) = sqrt((Δx / y)^2 + (Δy*x / y^2)^2)
    πρ  = mπ / mρ
    πL  = L*mπ
    Δπρ = Δratio(mπ,mρ,Δmπ,Δmρ)
    ΔπL = L*Δmπ
    # and human readable strings
    sπρ = errorstring(πρ,Δπρ)
    sπ  = errorstring(mπ,Δmπ)
    sρ  = errorstring(mρ,Δmρ)
    sη  = errorstring(mη,Δmη)
    sσ  = errorstring(mσ,Δmσ)
    sa0 = errorstring(ma0,Δma0)
    sπL = errorstring(πL,ΔπL)
    sP  = errorstring(P,ΔP)
    # set up csv's for
    ioPR = open("DatafreezeSinglets/data/"*filedat*".csv","a+")
    iszero(position(ioPR)) && write(ioPR,"β;m;L;T;Nconf;Nhits;fitη;fitπ;fit_sigma;fit_a0;fit_rho;mπ;Δmπ;mρ;Δmρ;ma0;Δma0;mη;Δmη;mσ;Δmσ;P;ΔP;mπ/mρ;Δ(mπ/mρ);mπL;ΔmπL\n")
    write(ioPR,"$β;$(m[1]);$L;$T;$Nconf;$hits;$fitη;$fitπ;$fitσ;$fita0;$fitρ;$mπ;$Δmπ;$mρ;$Δmρ;$ma0;$Δma0;$mη;$Δmη;$mσ;$Δmσ;$P;$ΔP;$πρ;$Δπρ;$πL;$ΔπL\n")
    close(ioPR)
    ioPR = open("DatafreezeSinglets/data_human_readable/"*filedat*"_HR.csv","a+")
    iszero(position(ioPR)) && write(ioPR,"β;m;L;T;Nconf;hits;fitη;fitπ;fit_sigma;fit_a0;fit_rho;mπ;mρ;ma0;mη;mσ;P;mπ/mρ;mπL\n")
    write(ioPR,"$β;$(m[1]);$L;$T;$Nconf;$hits;$fitη;$fitπ;$fitσ;$fita0;$fitρ;$sπ;$sρ;$sa0;$sη;$sσ;$sP;$sπρ;$sπL\n")
    close(ioPR)
end

files = readdir("DatafreezeSinglets/parameters/";join=true)
for prmfile in files[1:8]
    #prmfile = "DatafreezeSinglets/parameters/param_deriv.csv"
    contains(prmfile,"non_deg") && continue
    for i in 2:countlines(prmfile)
        id = replace(first(splitext(basename(prmfile))),"param_" => "")
        odir, hits, fitη, fitπ, fitσ, fita0, fitρ, fileM, fileS, group, name, vsub, deriv, gs_sub, constant = read_prm(prmfile,i)

        fileid = string(joinpath(splitpath(odir)[end-2:end-1]))
        save = false
        write = true
        minplateau = 4

        @show fileM
        Nconf, m, G, β = nconfigs(fileM),quarkmasses(fileM),gaugegroup(fileM),couplingβ(fileM)
        G == "SU(3)" && continue

        kws = (therm=0,step=1,autocor=true)
        T, L = latticesize(fileM)[1:2]
        typeM = "DEFAULT_SEMWALL TRIPLET"
        mπ, Δmπ = meson_mass_decay_select(fileM,"g5",typeM;error=:jack,nexp2=false,ncut=fitπ  ,kws...)[1:2]
        mρ, Δmρ = meson_mass_decay_select(fileM,"g1",typeM;error=:jack,nexp2=false,ncut=fitρ  ,kws...)[1:2]
        if fita0 != -1
            ma, Δma = meson_mass_decay_select(fileM,"id",typeM;error=:jack,nexp2=false,ncut=fita0 ,kws...)[1:2]
        else
            ma, Δma = NaN, NaN
        end

        T, L = latticesize(fileM)[1:2]
        rescale = (L^3)^2 /L^3
        type = "DISCON_SEMWALL SINGLET" 
        P, ΔP = average_plaquette(fileM)

        if fitη != -1
            key, state, sigma = "g5", L"\eta'", false         
            mη, Δmη, meffη, Δmeffη, cη, Δcη, C, ΔC, Cd, ΔCd = singlet_analysis(fileM,fileS,hits,vsub,key,fitπ,fitη,deriv,gs_sub,sigma,constant)
            #plt1 = singlet_effective_mass_plot(meffη,Δmeffη,mπ,Δmπ,mη,Δmη,mρ,Δmρ,key,fitη,deriv,fileM,hits,state;save)
            #display(plt1) 
        end
        if fitσ != -1 
            if fita0 == -1
                gs_sub = false
            end
            key, state, sigma, constant = "id", L"\sigma", true, false
            mσ, Δmσ, meffσ, Δmeffσ, cσ, Δcσ, C, ΔC, Cd, ΔCd = singlet_analysis(fileM,fileS,hits,vsub,key,fita0,fitσ,deriv,gs_sub,sigma,constant)
            #plt2 = singlet_effective_mass_plot(meffσ,Δmeffσ,mπ,Δmπ,mσ,Δmσ,mρ,Δmρ,key,fitσ,deriv,fileM,hits,state;save,fit=true,connected=true,update_limit=false)        
            #display(plt2) 
        end

        if (fitη == -1) || (fitη[2] - fitη[1] < minplateau - 1) 
            mη, Δmη = NaN, NaN
        end
        if (fitσ == -1) || (fitσ[2] - fitσ[1] < minplateau - 1)
            mσ, Δmσ = NaN, NaN
        end
        write && write_singlet_files("data_$id","param_$id",fileM,fileS,fitη,fitπ,fitσ,fita0,fitρ,mπ,Δmπ,mρ,Δmρ,ma,Δma,mη,Δmη,mσ,Δmσ,fileid,hits,typeM,P,ΔP,group,name,vsub,deriv,gs_sub,constant)
    end
end