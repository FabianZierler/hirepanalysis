using HiRepAnalysis
using HDF5
using DelimitedFiles
include("writeHDF5.jl")
function fermion_masses_from_filename(file)
    p1 = last(findfirst("m1",file)) 
    p2 = first(findfirst("m2",file))
    p3 = last(findfirst("m2",file)) 
    p4 = findnext('/',file,p3) 
    # create array of masses for matching of output
    m  = [file[p1+1:p2-1],file[p3+1:p4-1]] 
    return m
end
function logfiles_to_hdf5(name,path,hdfpath,fileC,typeC,typeD)
    dir = joinpath(path,name) 
    discon_logs = filter(contains("discon"),readdir(dir,join=true))
    # the fifth element after splitting contains the number of hits
    hits_strings = getindex.(split.(basename.(discon_logs),"_"),4)
    discon_hits  = parse.(Int,replace.(hits_strings,"h"=>""))
    # restrict ourselves to scalar and pseudoscalar data
    only_channels = ["id_disc_re", "g5_disc_re"]
    writehdf5_spectrum_connected(fileC,typeC;abspath=hdfpath)
    for (log,hits) in zip(discon_logs,discon_hits)
        writehdf5_spectrum_disconnected(log,typeD,hits;abspath=hdfpath,only_channels)
    end
end
# merge all generated hdf5 files for disconnected contributions
function merge_all_hdf5_files(name,hdfpath,typeD)
    dir = joinpath(hdfpath,name)
    discon_hdf5 = filter(contains("discon"),readdir(dir,join=true))
    tmpname = joinpath(dir,"tmp.hdf5")
    dstname = joinpath(dir,"out_spectrum_discon.hdf5")
    for (i,log) in enumerate(discon_hdf5)
        if i==1
            if length(discon_hdf5) > 1
                mergehdf_discon(tmpname,typeD,log)
            else
                mergehdf_discon(dstname,typeD,log)
            end
        else
            mergehdf_discon(dstname,typeD,log,tmpname)
            cp(dstname,tmpname;force=true)
        end
    end
    isfile(tmpname) && rm(tmpname)
    rm.(discon_hdf5)
end

namesDeg0    = readdlm("DatafreezeSinglets/parameters/param_both.csv",';';skipstart=1)[:,1]
namesNonDeg0 = readdlm("DatafreezeSinglets/parameters/param_non_deg.csv",';';skipstart=1)[:,1]
hitsNonDeg0  = readdlm("DatafreezeSinglets/parameters/param_non_deg.csv",';';skipstart=1)[:,2]
namesDeg     = @. joinpath(namesDeg0,"out")
namesNonDeg  = @. joinpath(namesNonDeg0,"out")

type   = "DISCON_SEMWALL SINGLET"
typeC  = "DEFAULT_SEMWALL TRIPLET"
typeU  = "SEMWALL_U TRIPLET"
typeD  = "SEMWALL_D TRIPLET"
typeUD = "SEMWALL_UD TRIPLET"
filesC1 = joinpath.(path,namesDeg0,"out","out_spectrum")
filesC2 = joinpath.(path,namesNonDeg0,"out","out_spectrum_with_pcac")

files = readdlm("spectrum_logfiles_list")[:,1]

# special case degenerate limit for non-degenerate data
writehdf5_spectrum_connected(filesC2[8],typeC;abspath=hdfpath)
# non-degenerate output files
for i in eachindex(namesNonDeg[1:7])
    hits  = hitsNonDeg0[i]
    fileD = joinpath(path,namesNonDeg[i],"out_spectrum_discon_h$hits")
    fileDtmp = joinpath(hdfpath,namesNonDeg[i],"out_spectrum_discon_h$hits.hdf5")
    fileDout = joinpath(hdfpath,namesNonDeg[i],"out_spectrum_discon.hdf5")
    masses = fermion_masses_from_filename(fileD)
    writehdf5_spectrum_disconnected_nondeg(fileD,type,hits,masses;abspath=hdfpath,only_channels=["id_disc_re", "g5_disc_re"])
    mv(fileDtmp,fileDout)
    writehdf5_spectrum_connected(filesC2[i],typeU,typeD,typeUD;abspath=hdfpath)
end
# degenerate output files
for (i,name) in enumerate(namesDeg)
    logfiles_to_hdf5(name,path,hdfpath,filesC1[i],typeC,type)
    merge_all_hdf5_files(name,hdfpath,type)
end