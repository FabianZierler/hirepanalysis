using Plots
using HiRepAnalysis
using LinearAlgebra
using LaTeXStrings
using DelimitedFiles
pgfplotsx(legendfontsize=14,labelfontsize=20,tickfontsize=14,titlefontsize=16,ms=4,framestyle=:box,legend=:outerright)
#plotly(legend=:none)

parse_fit_intervall(s) = isa(s,Integer) ? s : Tuple(parse.(Int,split(s[2:end-1],',')))
function read_prm_nondeg(file,i)
    prm = readdlm(file,';')
    odir = joinpath(path,prm[i,1],"out")
    hits = prm[i,2]
    fitη, fitπ0, fitπc, fitσ, fita0, fitρ = parse_fit_intervall.(prm[i,3:8])
    name = prm[i,10]
    return odir, hits, fitη, fitπ0, fitπc, fitσ, fita0, fitρ, name
end
function fermion_masses_from_filename(file)
    p1 = last(findfirst("m1",file)) 
    p2 = first(findfirst("m2",file))
    p3 = last(findfirst("m2",file)) 
    p4 = findnext('/',file,p3) 
    # create array of masses for matching of output
    m  = [file[p1+1:p2-1],file[p3+1:p4-1]] 
    return m
end
function filter_meff_for_plot!(meffη,Δmeffη,mπ)
    for i in eachindex(Δmeffη)
        if Δmeffη[i] > meffη[i] || Δmeffη[i] > 2mπ  
            meffη[i], Δmeffη[i] = NaN, NaN
        end
        if meffη[i] < 0.01
            meffη[i], Δmeffη[i] = NaN, NaN
        end
    end
end

prmfile = "DatafreezeSinglets/parameters/param_non_deg.csv"
for i in 2:countlines(prmfile)
    odir, hits, fitη, fitπ0, fitπc, fitσ, fita0, fitρ, ensemble_name = read_prm_nondeg(prmfile,i)
    m = fermion_masses_from_filename(odir)

    type    = "DISCON_SEMWALL SINGLET"
    typeM   = "DEFAULT_SEMWALL TRIPLET"
    typeU   = "SEMWALL_U TRIPLET"
    typeD   = "SEMWALL_D TRIPLET"
    typeUD  = "SEMWALL_UD TRIPLET"
    fileM   = joinpath(odir,"out_spectrum_with_pcac.hdf5")
    fileS   = joinpath(odir,"out_spectrum_discon.hdf5")

    T = latticesize(fileM)[1]
    L = latticesize(fileM)[2]
    rescale = (L^3)^2 /L^3
    writefile = true

    # obtain disconnected part
    if m[1] != m[2]
        C_dis_MC_eta = disconnected_eta_MC(fileS,type,hits,m[1],m[2];rescale=rescale,key="g5_disc_re",vsub=false)
        C_dis_MC_pi0 = disconnected_pi0_MC(fileS,type,hits,m[1],m[2];rescale=rescale,key="g5_disc_re",vsub=false)
        C_dis_MC_sigma = disconnected_eta_MC(fileS,type,hits,m[1],m[2];rescale=rescale,key="id_disc_re",vsub=false)
        # obtain connected part
        C_con_MC = correlators(fileM,typeU,typeD,"g5")
        C_con_MC_sigma = correlators(fileM,typeU,typeD,"id")
        HiRepAnalysis._rescale_corrs!(C_con_MC, L)
        # obtain non-singlet masses
        kws = (error=:jack,nexp2=false)
        mπc, Δmπc = meson_mass_decay(fileM,"g5",typeU,typeD;ncut=fitπc,kws...)[1:2]
        mρc, Δmρc = meson_mass_decay(fileM,"g1",typeU,typeD;ncut=fitρ,kws...)[1:2]
        mπf, Δmπf = meson_mass_decay(fileM,"g5",typeUD;ncut=fitπc,kws...)[1:2]
        mρf, Δmρf = meson_mass_decay(fileM,"g1",typeUD;ncut=fitρ,kws...)[1:2]
        #obtain PCAC/AWI mass
        kwsAWI = (therm=0,step=1,autocor=false,ncut=fitπc[1])
        mAWI, ΔmAWI = awi_mass(fileM,typeUD;kwsAWI...)
    else
        C_dis_MC_eta = disconnected_eta_MC(fileS,type,hits;rescale=rescale,key="g5_disc_re",vsub=false)
        C_dis_MC_pi0 = zero(C_dis_MC_eta)
        C_dis_MC_sigma = disconnected_eta_MC(fileS,type,hits;rescale=rescale,key="id_disc_re",vsub=false)
        # obtain connected part
        C_con_MC = correlators(fileM,typeM,"g5")
        C_con_MC_sigma = correlators(fileM,typeM,"id")
        HiRepAnalysis._rescale_corrs!(C_con_MC, L)
        #obtain PCAC/AWI mass
        kwsAWI = (therm=0,step=1,autocor=false,ncut=fitπc[1])
        mAWI, ΔmAWI = awi_mass(fileM,typeM;kwsAWI...)
        # obtain non-singlet masses
        kws = (error=:jack,nexp2=false)
        mπf, Δmπf = meson_mass_decay(fileM,"g5",typeM;ncut=fitπc,kws...)[1:2]
        mρf, Δmρf = meson_mass_decay(fileM,"g1",typeM;ncut=fitρ,kws...)[1:2]
        mπc, Δmπc = mπf, Δmπf
        mρc, Δmρc = mρf, Δmρf 
    end

    if size(C_dis_MC_eta)[1] != size(C_con_MC)[2] 
        @warn "mismatch between connected  $(size(C_con_MC)[2]) and disconnected $(size(C_dis_MC_eta)[1])"
    end
    if fitπ0 != -1
        mπ0, Δmπ0, Cdπ0 ,ΔCdπ0, Cπ0, ΔCπ0, meffπ0, Δmeffπ0, cπ0, Δcπ0 = singlet_jackknife(C_con_MC,C_dis_MC_pi0,fitπc,fitπ0;deriv=true,gs_sub=true,sigma=false,constant=false)
    else
        mπ0, Δmπ0 = NaN,NaN
    end
    if fitη != -1
        mη, Δmη, Cdη, ΔCdη, Cη, ΔCη, meffη, Δmeffη, cη, Δcη = singlet_jackknife(C_con_MC,C_dis_MC_eta,fitπc,fitη;deriv=true,gs_sub=true,sigma=false,constant=false)
    else
        mη,  Δmη = NaN,NaN
    end
    if fitσ != -1
        mσ, Δmσ, Cdσ, ΔCdσ, Cσ, ΔCσ, meffσ, Δmeffσ, cσ, Δcσ = singlet_jackknife(C_con_MC_sigma,C_dis_MC_sigma,fita0,fitσ;deriv=true,gs_sub=true,sigma=true,constant=false)
    else
        mσ, Δmσ = NaN,NaN
    end

    G = gaugegroup(fileM)
    β = couplingβ(fileM)
    maxconf = HiRepAnalysis.nconfigs(fileM)
    title=L"$ %$(T)\times %$(L)^3, \beta=%$β, %$G, m_q=%$(first(m)),%$(last(m))$, $nconf = %$maxconf$"
    name="$(T)x$(L)^3 $G, m=$(first(m)),$(last(m))"
    fileid = string(joinpath(splitpath(odir)[end-2:end-1]))

    P, ΔP = average_plaquette(fileM)

    sπc = errorstring(mπc,Δmπc)
    sρc = errorstring(mρc,Δmρc)
    sπf = errorstring(mπf,Δmπf)
    sρf = errorstring(mρf,Δmρf)
    sη  = errorstring(mη,Δmη)
    sσ  = errorstring(mσ,Δmσ)
    sπ0 = errorstring(mπ0,Δmπ0)
    # calculate ratios 
    Δratio(x,y,Δx,Δy) = sqrt((Δx / y)^2 + (Δy*x / y^2)^2)
    ππ  = mπf / mπ0
    πL  = L*mπ0
    Δππ = Δratio(mπf,mπ0,Δmπf,Δmπ0)
    ΔπL = L*Δmπ0
    # more errorstrings
    sππ = errorstring(ππ,Δππ)
    sπL = errorstring(πL,ΔπL)
    sP  = errorstring(P,ΔP)
    sAWI = errorstring(mAWI,ΔmAWI)

    if writefile
        T, L = latticesize(fileM)[1:2]
        Nconf, G, β = nconfigs(fileM),gaugegroup(fileM),couplingβ(fileM)
        #=ioPR = open("DatafreezeSinglets/parameters/param_non_deg.csv","a+")
        iszero(position(ioPR)) && write(ioPR,"file;hits;fitη;fitπ0;fitπc;fitσ;fita0;fitρ;Nconf;name;\n")
        write(ioPR,"$fileid;$hits;$fitη;$fitπ0;$fitπc;$fitσ;$fita0;$fitρ;$Nconf;$ensemble_name\n")
        close(ioPR)=#
        ioPR = open("DatafreezeSinglets/data/data_non_deg.csv","a+")
        iszero(position(ioPR)) && write(ioPR,"β;m1;m2;L;T;Nconf;hits;fitη;fitπ0;fitπc;fitσ;fita0;fitρ;P;ΔP;mπ0;Δmπ0;mπc;Δmπc;mπf;Δmπf;mρ0;Δmρ0;mρf;Δmρf;mσ;Δmσ;mη;Δmη;mAWI;ΔmAWI\n")
        write(ioPR,"$β;$(m[1]);$(m[2]);$L;$T;$Nconf;$hits;$fitη;$fitπ0;$fitπc;$fitσ;$fita0;$fitρ;$P;$ΔP;$mπ0;$Δmπ0;$mπc;$Δmπc;$mπf;$Δmπf;$mρc;$Δmρc;$mρf;$Δmρf;$mσ;$Δmσ;$mη;$Δmη;$mAWI;$ΔmAWI\n")
        close(ioPR)
        ioPR = open("DatafreezeSinglets/data_human_readable/data_non_deg_HR.csv","a+")
        iszero(position(ioPR)) && write(ioPR,"β;m1;m2;L;T;Nconf;hits;fitη;fitπ0;fitπc;fitσ;fita0;fitρ;P;mπ0;mπc;mπf;mρ0;mρf;mσ;mη;mAWI\n")
        write(ioPR,"$β;$(m[1]);$(m[2]);$L;$T;$Nconf;$hits;$fitη;$fitπ0;$fitπc;$fitσ;$fita0;$fitρ;$sP;$sπ0;$sπc;$sπf;$sρc;$sρf;$sσ;$sη;$sAWI\n")
        close(ioPR)
    end

    fη = first(fitη):last(fitη)
    fπ = first(fitπ0):last(fitπ0)
    fσ = first(fitσ):last(fitσ)
    t = 3:div(T,2)

    plot(title=title)
    if fitη != -1
        filter_meff_for_plot!(meffη,Δmeffη,mπc)
        scatter!(t,meffη[t],  yerr=Δmeffη[t], label=L"$\eta'$ eff. mass", markershape=:circle)
        plot!(fη,mη*ones(length(fη)),ribbon=Δmη,markershape=:none,label=L"$m_{\eta'}=%$sη$")
    end
    if fitπ0 != -1
        scatter!(t,meffπ0[t], yerr=Δmeffπ0[t], label=L"$\pi^0$ eff. mass", markershape=:square)
        plot!(fπ,mπ0*ones(length(fπ)),ribbon=Δmπ0,markershape=:none,label=L"$m_{\pi^0}=%$sπ0$")
    end
    if fitσ != -1
        filter_meff_for_plot!(meffσ,Δmeffσ,mπc)
        scatter!(t,meffσ[t], yerr=Δmeffσ[t], label=L"$\sigma$ eff. mass", markershape=:square)
        plot!(fσ,mσ*ones(length(fσ)),ribbon=Δmσ,markershape=:none,label=L"$m_{\sigma}=%$sσ$")
    end
    plot!(1:T,mρc*ones(T),ribbon=Δmρc,label=L"$m_{\rho^0}=%$sρc$",markershape=:none)
    plot!(1:T,mπc*ones(T),ribbon=Δmπc,label=L"$m_{\pi^0_c}=%$sπc$",markershape=:none)
    plot!(1:T,mρf*ones(T),ribbon=Δmρf,label=L"$m_{\rho^\pm}=%$sρf$",markershape=:none)
    plot!(1:T,mπf*ones(T),ribbon=Δmπf,label=L"$m_{\pi^\pm}=%$sπf$",markershape=:none)
    plot!(xlims=(1.5,T/2 + 0.5),xticks=2:div(T,2))
    plt = plot!(xlabel=L"t/a", ylabel=L"a m_{\rm eff}")
    display(plt)
end

