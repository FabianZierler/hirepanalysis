#cd("/home/fabian/.julia/dev/hirepanalysis")
cd("/home/zierler_fabian/.julia/dev/hirepanalysis")
using Pkg
Pkg.activate(".")
Pkg.instantiate()
path0 = "/mnt/87c7a72f-9c90-4810-81cc-37000e08b575/Singlets_Data/"
path0 = "/home/zierler_fabian/Documents/Singlets_Data/"
Qdir = joinpath(path0,"Qhistories")
hdfpath = joinpath(path0,"hdf5_files")

# first convert all log files to hdf5
path = path0
include("convertlog.jl")
# perform analysis of degenerate and non-degenerate mesons
path  = hdfpath
include("singlet_deg.jl")
include("singlet_nondeg.jl")
# then combine these results with the wilson flow results
# additionally split hdf5 files into files with fixed topological charge Q
path = joinpath(hdfpath,"runsSp4")
include("Qhistory.jl")
# write tables
include("singlet_tables.jl")
# create Plots
path  = hdfpath
include("plots_paper.jl")
# perform analysis with smeared correlators
# will this be part of the data release?
path = path0
include("analysis_smeared.jl")