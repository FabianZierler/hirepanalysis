using DelimitedFiles
using Plots
using LaTeXStrings
pgfplotsx(ms=6,legendfontsize=16,titlefontsize=20,labelfontsize=20,tickfontsize=20,framestyle=:box)
data = readdlm("/home/zierler_fabian/Documents/ensembles.csv",';',skipstart=1)

x  = data[:,6]
Δx = data[:,7]
y  = data[:,8].*data[:,3]
Δy = data[:,9].*data[:,3]


beta   = data[:,1]
collab = data[:,end]
marker = @. ifelse(collab == "Graz",:circle,:square) 
colour = similar(beta)
for i in eachindex(beta)
    if beta[i] == 7.5
        colour[i] = :red
    elseif beta[i] == 7.4
        colour[i] = :green
    elseif beta[i] == 7.2
        colour[i] = :blue
    elseif beta[i] == 7.05
        colour[i] = :purple
    elseif beta[i] == 6.9
        colour[i] = :orange
    end
end
plt = scatter(x,xerr=Δx,y,yerr=Δy,yflip=true,markershape=marker,label="",mc=colour)
xlim = xlims(plt)
ylim = ylims(plt)
plt = plot()
scatter!(plt,[0],[0],markershape=:square,markercolor=:red,label=L"$\beta=7.5$(Swansea)")
scatter!(plt,[0],[0],markershape=:square,color=:green,label=L"$\beta=7.4$(Swansea)")
scatter!(plt,[0],[0],markershape=:square,color=:blue,label=L"$\beta=7.2$(Swansea)")
scatter!(plt,[0],[0],markershape=:circle,color=:blue,label=L"$\beta=7.2$(Graz)")
scatter!(plt,[0],[0],markershape=:square,color=:purple,label=L"$\beta=7.05$(Swansea)")
scatter!(plt,[0],[0],markershape=:circle,color=:purple,label=L"$\beta=7.05$(Graz)")
scatter!(plt,[0],[0],markershape=:square,color=:orange,label=L"$\beta=6.9$(Swansea)")
scatter!(plt,[0],[0],markershape=:circle,color=:orange,label=L"$\beta=6.9$(Graz)")
for i in eachindex(x)
    scatter!([x[i]],xerr=[Δx[i]],[y[i]],yerr=[Δy[i]],shape=marker[i],color=colour[i],label="")
end
scatter!(plt,ylims=ylim,xlims=xlim,legendfonthalign=:left,yflip=true)
scatter!(plt,title="available ensembles")
scatter!(plt,xlabel=L"$m_\pi /m_\rho$ on largest lattice")
scatter!(plt,ylabel=L"$m_\pi^\infty L$")
savefig(plt,"/home/zierler_fabian/Documents/ensembles_plot.pdf")
plt