using Roots
function scattering_length_from_lüscher(a0,δE,mπ,L)
    c1 = -2.837
    c2 = +6.375
    return δE/mπ - 4π*mπ*a0*(1.0 + c1*a0/L + c2 * a0^2/L^2)/(mπ*L)^3
end
δE  = -0.010
ΔδE =  0.004
L   =  12
mπ  =  0.5663
fπ  =  0.1052
Δfπ =  0.0013
Δmπ =  0.0008
a0_root(a0) = scattering_length_from_lüscher(a0,δE,mπ,L) 
a0_root_plus(a0) = scattering_length_from_lüscher(a0,δE+ΔδE,mπ,L) 
a0_root_minus(a0) = scattering_length_from_lüscher(a0,δE-ΔδE,mπ,L) 
a0 = find_zero(a0_root,-0.5)
a0_plus = find_zero(a0_root_plus,-0.5)
a0_minus = find_zero(a0_root_minus,-0.5)
abs(a0 - a0_plus)
abs(a0 - a0_minus)

a0_chiPT(mπ,fπ) = -mπ/(32*fπ^2)
Δa0_chiPT(mπ,fπ,Δmπ,Δfπ) = sqrt((Δmπ/32/fπ/fπ)^2 + (mπ*Δfπ/32/fπ^3)^2)
a0_chiPT(mπ,fπ)
Δa0_chiPT(mπ,fπ,Δmπ,Δfπ)

#using Plots
#x = -4.0:0.01:4.0
#y = scattering_length_from_lüscher.(x,δE,mπ,L)
#scatter(x,y)

