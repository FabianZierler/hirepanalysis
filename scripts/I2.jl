using HiRepAnalysis
using Statistics
using Parsers
path = "/media/fabian/HDD#3/outfiles/"
path = "/home/zierler_fabian/Documents/rsync/rsyncVSC/"
file = "/media/fabian/HDD#3/rsyncout/rsyncVSC/runsSp4/Lt24Ls14beta6.9m1-0.90m2-0.90/out/out_scattering"

function parse_I2_scattering(file)
    # get number of stochastics sources and time extent
    h = nhits(file)
    N = nconfigs(file)
    T = latticesize(file)[1]
    # init variuables for metadata
    src  = 0
    ncfg = 0
    cfg0 = "dummy"
    name = "channel_dummy"
    # Arrays for parsed data 
    pi1  = zeros(N,h,T)
    pi2  = zeros(N,h,T)
    AD = zeros(N,h,T)
    BC = zeros(N,h,T)
    # star parsing
    offset = length("[IO][0]")
    for line in eachline(file)
        if startswith(line,"[IO][0]")
            if startswith(line,"[IO][0]Configuration")
                continue
            end
            if !isdigit(line[offset+1])
                # parse metadata from outputfile
                posA = first(findfirst("src", line))
                posB = first(findfirst("run", line))
                name = line[offset+1:posA-2]
                src  = Parsers.parse(Int,line[posA+4:posB-2])+1
                cfg  = line[posB+3:end]
                # increment # of configuration if its name changes
                if cfg != cfg0
                    ncfg += 1
                    #@show ncfg
                    cfg0 = cfg
                end
            else
                # if data starts with a digit then this is the numerical data
                # parse it intop appropriate arrays 
                posA = findnext(' ',line,offset+1)
                #px = Parsers.parse(Int,line[offset+1:posA-1])
                posB = findnext(' ',line,posA+1)
                #py = Parsers.parse(Int,line[posA+1:posB-1])
                posA = findnext(' ',line,posB+1)
                #pz = Parsers.parse(Int,line[posB+1:posA-1])
                posB = findnext(' ',line,posA+1)
                t  = Parsers.parse(Int,line[posA+1:posB-1])+1
                posA = findnext(' ',line,posB+1)
                Re = Parsers.parse(Float64,line[posB+1:posA-1])
                #Im = Parsers.parse(Float64,line[posA+1:end])
                # write results int arrays
                (name == "AD")   && (AD[ncfg,src,t] = Re)
                (name == "BC")   && (BC[ncfg,src,t] = Re)
                (name == "pi1")  && (pi1[ncfg,src,t] = Re)
                (name == "pi2")  && (pi2[ncfg,src,t] = Re)
            end
        end
    end
    return AD, BC, pi1, pi2
end
AD, BC, pi1, pi2 = parse_I2_scattering(file)

using Statistics
# 1) add up AD and BC
# 2) average over all sources and then
# 3) average over number of configs
# 4) standard deviation of the mean wrt configurations
N, nsrc, T = size(AD)
Cpipi_src_cfg = AD + BC 
Cpipi_cfg = dropdims(mean(Cpipi_src_cfg,dims=2),dims=2)
Cpipi  = dropdims(mean(Cpipi_cfg,dims=1),dims=1)
ΔCpipi = dropdims(std(Cpipi_cfg,dims=1),dims=1)/sqrt(N)

# do the same fot the single-pion correlator 
Cpi_cfg = dropdims(mean(pi1,dims=2),dims=2)
Cpi  = dropdims(mean(Cpi_cfg,dims=1),dims=1)
ΔCpi = dropdims(std(Cpi_cfg,dims=1),dims=1)/(sqrt(N))

# effective mass shows kink at the center of the latticesize
# we attribute this to an extra constant in the correlator
# perform numerical derivative to remove it 
Cd_pipi_cfg = correlator_deriv(Cpipi_cfg)
Cd_pi_cfg   = correlator_deriv(Cpi_cfg)
Cd_pi       = dropdims(mean(Cd_pi_cfg,dims=1),dims=1)
Cd_pipi     = dropdims(mean(Cd_pipi_cfg,dims=1),dims=1)
ΔCd_pi      = dropdims(std(Cd_pi_cfg,dims=1),dims=1)/sqrt(N)
ΔCd_pipi    = dropdims(std(Cd_pipi_cfg,dims=1),dims=1)/sqrt(N)

using Plots
# visual inspection of the energy levels
scatter(Cpipi,yerr=ΔCpipi,yaxis=:log10)
# visually inspect effective mass based on apply_jackknife
meff1  = HiRepAnalysis.effectivemass_cosh(Cpipi)
Δmeff1 = HiRepAnalysis.effectivemass_cosh_err(Cpipi,ΔCpipi)
meff2  = HiRepAnalysis.effectivemass_sinh(Cd_pipi)
Δmeff2 = HiRepAnalysis.effectivemass_sinh_err(Cd_pipi,ΔCd_pipi)
meff1, Δmeff1 = HiRepAnalysis.implicit_meff_jackknife(Cpipi_cfg';sign=+1)
meff2, Δmeff2 = HiRepAnalysis.implicit_meff_jackknife(Cd_pipi_cfg';sign=-1)
# also obtain effective mass for single pion
meff3, Δmeff3 = HiRepAnalysis.implicit_meff_jackknife(Cpi_cfg';sign=+1)
meff4, Δmeff4 = HiRepAnalysis.implicit_meff_jackknife(Cd_pi_cfg';sign=-1)
meff3  = HiRepAnalysis.effectivemass_cosh(Cpi)
Δmeff3 = HiRepAnalysis.effectivemass_cosh_err(Cpi,ΔCpi)
meff4  = HiRepAnalysis.effectivemass_sinh(Cd_pi)
Δmeff4 = HiRepAnalysis.effectivemass_sinh_err(Cd_pi,ΔCd_pi)

plot()
#scatter!(meff1, yerr=Δmeff1)
scatter!(meff2, yerr=Δmeff2)
scatter!(2meff3, yerr=2Δmeff3)
#scatter!(2meff4, yerr=2Δmeff4)
ylims!(1.1,2)
xlims!(1,T/2)

# Questions
# 1) The constant terms appears to be quite an issue
# 2) Different subtraction methods appear favourable for different correlators?
# 3) This raises the question of compareability?

# What can we do?
# 1) Test it on larger T
# 2) Compare ΔE for different subtraction methods