using HiRepAnalysis
using Plots
include("MadrasSokal.jl")
plotlyjs()
path = "/home/fabian/Documents/Lattice/PlaquettesTursa/plaquettes/"
name = "plaquette_nt48l20"
name = "plaquette_nt64l20"
name = "plaquette_nt64l20amf0p70"
name = "plaquette_nt64l32amf0p72"
name = "plaquette_nt96l20"
name = "plaquette_nt80l20"
file = joinpath(path,name)

function plaquettes_grid(file)
    plaquettes = Float64[]
    configurations = Int64[]
    for line in eachline(file)
        p = findfirst("Plaquette",line)
        line = line[p[end]+2:end]
        line = replace(line,"[" =>" ")
        line = replace(line,"]" =>" ")
        data =  split(line)
        append!(configurations,parse(Int64,data[1]))
        append!(plaquettes,parse(Float64,data[2]))
    end
    perm = sortperm(configurations)
    permute!(plaquettes,perm)
    permute!(configurations,perm)
    return configurations, plaquettes
end

skip  = 1
therm = 6500
configurations, plaq = plaquettes_grid(file)
p = plaq[therm:end]
n = configurations[therm:end]
plt2 = serieshistogram(n,p,title=name)
plot!(plt2,ylims=extrema(p))

#=
therms = 0:50:length(plaq)/2
τint  = zeros(Float64,size(therms))
Δτint = similar(τint)
for j in eachindex(therms)
    p = plaq[1+therms[j]:skip:end]
    N = length(p)
    
    Γ = madras_sokal(p,length(p)-1)
    τint_window  = zero(Γ)
    Δτint_window = zero(Γ)
    for i in eachindex(Γ)
        τint_window[i]  = 1/2 + sum(Γ[1:i])/Γ[1]
        Δτint_window[i] = sqrt((4i+2)/N*τint_window[i]^2)
    end
    τint[j],W = findmax(abs.(τint_window))
    Δτint[j]  = Δτint_window[W]
end

plt1 = plot(therms,2τint,ribbon=2Δτint,label="")
plot!(plt1,title  = "Autocorrelation as a function of n_therm")
plot!(plt1,xlabel = "thermalisation")
plot!(plt1,ylabel = "2τint")
display(plt1)
=#