using HiRepAnalysis
using DelimitedFiles
using HDF5
# 
function Q_of_meas(conf_meas,conf,Qint)
    N = min(length(conf_meas),length(conf))
    Qs  = zeros(N)
    for ind in 1:N
        pos = findfirst(isequal(conf_meas[ind]),conf)
        Qs[ind] = isnothing(pos) ? NaN : Qint[pos]
    end
    return Qs
end
# read and parse both my and Ed's data
function Qs_and_index(file_Ed,file_hdf)
    dat  = readdlm(file_Ed,',',skipstart=1)
    conf = Int.(dat[:,1])
    Q    = dat[:,2]
    Qint = round.(Q)
    h5file = h5open(file_hdf)
    confh5 = read(h5file,"configurations")
    conf_meas = parse.(Int,last.(split.(confh5,'n')))

    # get all the indices where we have configurations of a given topological charge
    Qom = Q_of_meas(conf_meas,conf,Qint)
    voQ = filter(isfinite,unique(Qom))
    ind = [ findall(isequal(voQ[i]),Qom) for i in eachindex(voQ)]

    return voQ, ind
end
function splithdf5_Q(file1,type,Qs,ind;absQ=false)
    hdf5_f1 = h5open(file1, "r")
    measurements = filter( x -> contains(x,type), keys(hdf5_f1))
    for i in eachindex(ind)
        absQ && Qs[i] <= 0 && continue
        # create hdf file for fixed topological sector 
        # obtain path of the current
        dir = dirname(file1)
        if absQ
            filename = splitext(basename(file1))[1]*"_absQ$(abs(Int(Qs[i]))).hdf"
            if -Qs[i] ∈ Qs
                i_neg = findfirst(isequal(-Qs[i]),Qs)
                index = vcat(ind[i],ind[i_neg]) 
            else
                index = ind[i]
            end 
        else
            filename = splitext(basename(file1))[1]*"_Q$(Int(Qs[i])).hdf"
            index = ind[i]
        end
        file = joinpath(dir,"fixedQ",filename)
        ispath(dirname(file)) || mkpath(dirname(file))
        isfile(file) && rm(file)    
        fid = h5open(file,"cw")
        # loop over all unchanged entries measurements and merge
        identical = ["lofgile","sources","beta","gauge group","lattice","quarkmasses"]
        for property in identical
            if haskey(hdf5_f1,property)
                dat = read(hdf5_f1,property)
                write(fid,property,dat)
            end
        end
        for property in ["plaquette","configurations"]
            dat = read(hdf5_f1,property)
            write(fid,property,dat[index])
        end
        for Γ in measurements
            dat = read(hdf5_f1,Γ)
            # different dimensions for connected and disconnected measurements
            if ndims(dat) == 3
                write(fid,Γ,dat[index,:,:])
            elseif ndims(dat) == 2
                write(fid,Γ,dat[:,index])
            end
        end
        close(fid)
    end     
end

for ensemble in readdir(Qdir)
    file = joinpath(Qdir,ensemble)
    filehdfC = joinpath(path,"$ensemble/out/out_spectrum.hdf5")
    filehdfD = joinpath(path,"$ensemble/out/out_spectrum_discon.hdf5")

    @show file
    typeD = "DISCON_SEMWALL SINGLET"
    typeC = "DEFAULT_SEMWALL TRIPLET"

    for absQ in [false,true]
        voQ,ind = Qs_and_index(file,filehdfD)
        splithdf5_Q(filehdfD,typeD,voQ,ind;absQ)
        voQ,ind = Qs_and_index(file,filehdfC)
        splithdf5_Q(filehdfC,typeC,voQ,ind;absQ)
    end
end

# flow scale w0 encoded in first line
files = readdlm("DatafreezeSinglets/parameters/param_deriv.csv",';',skipstart=1)[9:end,1]
files = basename.(files)

first_lines = readline.(joinpath.(Ref(Qdir),files))
w0strings = getindex.(split.(first_lines,Ref(('(',')','='))),3)
w0_with_uncertainty = split.(w0strings,Ref("+/-"))
w0  = parse.(Float64,getindex.(w0_with_uncertainty,1))
# combine this data with results from analysis in the julia scripts
data,header = readdlm("DatafreezeSinglets/data/data_deriv.csv",';',header=true)
Δw0 = parse.(Float64,getindex.(w0_with_uncertainty,2))
# remove all SU(2) simulations because we did not perform the gradient flow measurements there
# we use that all SU(2) configurations have β=2.0
data = data[9:end,:]
# now combine results: 1st header, then data
new_header = reshape(append!(vec(header),("w0","Deltaw0")),(1,29))
new_data   = hcat(data,w0,Δw0)
data_with_header = vcat(new_header,new_data)
writedlm("DatafreezeSinglets/data_flow.csv",data_with_header,';')